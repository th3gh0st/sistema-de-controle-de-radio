﻿namespace radio
{
    partial class Alocutar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TsStatusBanco = new System.Windows.Forms.ToolStripStatusLabel();
            this.DgPropaganda = new System.Windows.Forms.DataGridView();
            this.Propaganda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vezes_Ar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data_ini = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data_fim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tempo_init = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tempo_fim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Locutor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Detalhes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Secundagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtAtualizar = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgPropaganda)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(799, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 385);
            this.button1.TabIndex = 2;
            this.button1.Text = "INICIAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.TsStatusBanco});
            this.statusStrip1.Location = new System.Drawing.Point(0, 459);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(886, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(43, 17);
            this.toolStripStatusLabel1.Text = "Banco:";
            // 
            // TsStatusBanco
            // 
            this.TsStatusBanco.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.TsStatusBanco.LinkColor = System.Drawing.Color.Red;
            this.TsStatusBanco.Name = "TsStatusBanco";
            this.TsStatusBanco.Size = new System.Drawing.Size(42, 17);
            this.TsStatusBanco.Text = "Online";
            // 
            // DgPropaganda
            // 
            this.DgPropaganda.AllowUserToAddRows = false;
            this.DgPropaganda.AllowUserToDeleteRows = false;
            this.DgPropaganda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DgPropaganda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.DgPropaganda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgPropaganda.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Propaganda,
            this.Id,
            this.Vezes_Ar,
            this.Cliente,
            this.Data_ini,
            this.Data_fim,
            this.tempo_init,
            this.Tempo_fim,
            this.Locutor,
            this.Detalhes,
            this.Secundagem});
            this.DgPropaganda.Location = new System.Drawing.Point(12, 12);
            this.DgPropaganda.Name = "DgPropaganda";
            this.DgPropaganda.ReadOnly = true;
            this.DgPropaganda.Size = new System.Drawing.Size(771, 433);
            this.DgPropaganda.TabIndex = 13;
            // 
            // Propaganda
            // 
            this.Propaganda.HeaderText = "Propaganda";
            this.Propaganda.Name = "Propaganda";
            this.Propaganda.ReadOnly = true;
            this.Propaganda.Width = 90;
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            this.Id.Width = 41;
            // 
            // Vezes_Ar
            // 
            this.Vezes_Ar.HeaderText = "Vezes ao Ar";
            this.Vezes_Ar.Name = "Vezes_Ar";
            this.Vezes_Ar.ReadOnly = true;
            this.Vezes_Ar.Width = 89;
            // 
            // Cliente
            // 
            this.Cliente.HeaderText = "Cliente";
            this.Cliente.Name = "Cliente";
            this.Cliente.ReadOnly = true;
            this.Cliente.Width = 64;
            // 
            // Data_ini
            // 
            this.Data_ini.HeaderText = "Data Inicio";
            this.Data_ini.Name = "Data_ini";
            this.Data_ini.ReadOnly = true;
            this.Data_ini.Width = 83;
            // 
            // Data_fim
            // 
            this.Data_fim.HeaderText = "Data Fim";
            this.Data_fim.Name = "Data_fim";
            this.Data_fim.ReadOnly = true;
            this.Data_fim.Width = 74;
            // 
            // tempo_init
            // 
            this.tempo_init.HeaderText = "Hora Inicial";
            this.tempo_init.Name = "tempo_init";
            this.tempo_init.ReadOnly = true;
            this.tempo_init.Width = 85;
            // 
            // Tempo_fim
            // 
            this.Tempo_fim.HeaderText = "Hora Final";
            this.Tempo_fim.Name = "Tempo_fim";
            this.Tempo_fim.ReadOnly = true;
            this.Tempo_fim.Width = 80;
            // 
            // Locutor
            // 
            this.Locutor.HeaderText = "Locutor";
            this.Locutor.Name = "Locutor";
            this.Locutor.ReadOnly = true;
            this.Locutor.Width = 68;
            // 
            // Detalhes
            // 
            this.Detalhes.HeaderText = "Detalhes";
            this.Detalhes.Name = "Detalhes";
            this.Detalhes.ReadOnly = true;
            this.Detalhes.Visible = false;
            this.Detalhes.Width = 74;
            // 
            // Secundagem
            // 
            this.Secundagem.HeaderText = "Secundagem";
            this.Secundagem.Name = "Secundagem";
            this.Secundagem.ReadOnly = true;
            this.Secundagem.Width = 95;
            // 
            // BtAtualizar
            // 
            this.BtAtualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtAtualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtAtualizar.Location = new System.Drawing.Point(799, 12);
            this.BtAtualizar.Name = "BtAtualizar";
            this.BtAtualizar.Size = new System.Drawing.Size(75, 32);
            this.BtAtualizar.TabIndex = 15;
            this.BtAtualizar.Text = "Atualizar";
            this.BtAtualizar.UseVisualStyleBackColor = true;
            this.BtAtualizar.Click += new System.EventHandler(this.BtAtualizar_Click);
            // 
            // Alocutar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 481);
            this.Controls.Add(this.BtAtualizar);
            this.Controls.Add(this.DgPropaganda);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.button1);
            this.MinimumSize = new System.Drawing.Size(584, 300);
            this.Name = "Alocutar";
            this.Text = "Alocutar";
            this.Load += new System.EventHandler(this.Alocutar_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgPropaganda)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel TsStatusBanco;
        private System.Windows.Forms.DataGridView DgPropaganda;
        private System.Windows.Forms.Button BtAtualizar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Propaganda;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vezes_Ar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data_ini;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data_fim;
        private System.Windows.Forms.DataGridViewTextBoxColumn tempo_init;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tempo_fim;
        private System.Windows.Forms.DataGridViewTextBoxColumn Locutor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detalhes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Secundagem;
    }
}