﻿using MySql.Data.MySqlClient;
using radio.Properties;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace radio.banco
{
    public static class Gerenciador {
        public static Banco local;
        public static Banco online;

        private static int TimerRepeated = 0;
        static System.Timers.Timer time = new System.Timers.Timer();
        static System.Timers.Timer timeSinc  = new System.Timers.Timer();
        public static event EventHandler<StatusEventArgs> StatusChanged;
        public static String StatusOnline = "Tentando conectar...";
        public static void Sincronizar()
        {
            using (IDbCommand dbCommand = Gerenciador.online.Command("SELECT * FROM usuarios"))
            {
                try
                {
                    var result = dbCommand.ExecuteReader();
                    string sql = "INSERT INTO usuarios (Id, Login, Email, Senha, Tipo) VALUES";
                    int pos = 0;
                    Dictionary<string, object> values = new Dictionary<string, object>();
                    while (result.Read())
                    {                        
                        if (pos>=1)
                        {
                           
                            sql += " , ";
                        }
                        sql += "(@Id"+ pos + ",@Login" + pos + ", @Email" + pos + ", @Senha" + pos + ", @Tipo" + pos + ")";
                        values["@Id" + pos] = result["Id"];
                        values["@Login" + pos] = result["Login"];
                        values["@Email" + pos] = result["Email"];
                        values["@Senha" + pos] = result["Senha"];
                        values["@Tipo" + pos] = result["Tipo"];
                        pos += 1;
                    }
                    result.Close();
                    try
                    {
                        Gerenciador.local.Command("DELETE FROM usuarios").ExecuteNonQuery();
                    }
                    catch { }
                    
                    if (pos >= 1)
                    {                        
                        var command = Gerenciador.local.Command(sql);
                        foreach (var item in values)
                        {
                            var p = command.CreateParameter();
                            p.ParameterName = item.Key;
                            p.Value = item.Value;
                            command.Parameters.Add(p);
                        }
                        command.ExecuteNonQuery();
                    }
                }
                catch (MySqlException)
                {                   
                }
            }
            using (IDbCommand dbCommand = Gerenciador.online.Command("SELECT * FROM tipousu"))
            {
                try
                {
                    var result = dbCommand.ExecuteReader();
                    string sql = "INSERT INTO tipousu (Id, TipoUsu) VALUES ";
                    int pos = 0;
                    Dictionary<string, object> values = new Dictionary<string, object>();
                    while (result.Read())
                    {
                        if (pos >= 1)
                        {
                            sql += " , ";
                        }
                        sql += " (@Id" + pos + ",@TipoUsu" + pos + " )";
                        values["@Id" + pos] = result["Id"];
                        values["@TipoUsu" + pos] = result["TipoUsu"];                      
                        pos += 1;
                    }
                    result.Close();
                    try { 
                    Gerenciador.local.Command("DELETE tipousu").ExecuteNonQuery();
                }
                catch { }
                if (pos >= 1)
                    {                        
                        var command = Gerenciador.local.Command(sql);
                        foreach (var item in values)
                        {
                            var p = command.CreateParameter();
                            p.ParameterName = item.Key;
                            p.Value = item.Value;
                            command.Parameters.Add(p);
                        }
                        command.ExecuteNonQuery();
                    }
                }
                catch (MySqlException)
                {
                }
            }
            using (IDbCommand dbCommand = Gerenciador.online.Command("SELECT * FROM propagandas"))
            {
                try
                {
                    var result = dbCommand.ExecuteReader();
                    string sql = "INSERT INTO propagandas (Id, Propaganda, Cliente, Radio, Repetir, Data_ini, Data_fim, Vezes_Ar, Secundagem, Tempo_ini, Tempo_fim, nome, Locutor, Aviso, Observacoes) VALUES";
                    int pos = 0;
                    Dictionary<string, object> values = new Dictionary<string, object>();
                    while (result.Read())
                    {
                        if (pos >= 1)
                        {
                            sql += " , ";
                        }
                       sql += " (@Id" + pos + ",@Propaganda" + pos + ",@Cliente" + pos + ",@Radio" + pos + ",@Repetir" + pos + ",@Data_ini" + pos + ",@Data_fim" + pos + ",@Vezes_Ar" + pos + ",@Secundagem" + pos + ",@Tempo_ini" + pos + ",@Tempo_fim" + pos + ",@nome" + pos + ",@Locutor" + pos + ",@Aviso" + pos + ",@Observacoes" + pos + " )";
                        values["@Id" + pos] = result["Id"];
                        values["@Propaganda" + pos] = result["Propaganda"];
                        values["@Cliente" + pos] = result["Cliente"];
                        values["@Radio" + pos] = result["Radio"];
                        values["@Repetir" + pos] = result["Repetir"];
                        values["@Data_ini" + pos] = result["Data_ini"];
                        values["@Data_fim" + pos] = result["Data_fim"];
                        values["@Vezes_Ar" + pos] = result["Vezes_Ar"];
                        values["@Secundagem" + pos] = result["Secundagem"];
                        values["@Tempo_ini" + pos] = result["Tempo_ini"];
                        values["@Tempo_fim" + pos] = result["Tempo_fim"];
                        values["@nome" + pos] = result["nome"];
                        values["@Locutor" + pos] = result["Locutor"];
                        values["@Aviso" + pos] = result["Aviso"];
                        values["@Observacoes" + pos] = result["Observacoes"];
                        pos += 1;
                    }
                    result.Close();
                    try
                    {

                    Gerenciador.local.Command("DELETE propagandas").ExecuteNonQuery();
                    }
                    catch { }
                    if (pos >= 1)
                    {
                        
                        var command = Gerenciador.local.Command(sql);
                        foreach (var item in values)
                        {
                            var p = command.CreateParameter();
                            p.ParameterName = item.Key;
                            p.Value = item.Value;
                            command.Parameters.Add(p);
                        }
                        command.ExecuteNonQuery();
                    }
                }
                catch (MySqlException)
                {
                }
            }

        }


        private static void OnStatusEvent(object sender, StatusEventArgs e)
        {
            //if (e.Status == "Conectado")
            //{
            //    timeSinc.Start();
            //}
            //else
            //{
            //    timeSinc.Stop();
            //}
        }


        public static void iniciar() {
            StatusChanged += new EventHandler<StatusEventArgs>(OnStatusEvent);
            time.AutoReset = false;
            time.Interval = 1000;
            time.Elapsed += OnTimedEvent;

            timeSinc.AutoReset = false;
            timeSinc.Interval = 1000;
            timeSinc.Elapsed += OnTimedSincEvent;

            Dictionary<string, string> mConfig = new Dictionary<string, string>();
            mConfig["type"] = "sql";
            mConfig["conn"] = Program.connectionStringsSection.ConnectionStrings["BancoOff"].ConnectionString;
            local = new Banco(mConfig);

            try
            {
                local.conectar();
            }
            catch (SqlException)
            {
                MessageBox.Show("Não Foi possivel conectar com o banco local!", "Erro no banco Local", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            mConfig = new Dictionary<string, string>();
            mConfig["type"] = "mysql";
            mConfig["conn"] = Program.connectionStringsSection.ConnectionStrings["BancoOn"].ConnectionString;
            online = new Banco(mConfig);        
            
            online.conexao.StateChange += ConnectionStateOnline;
            online.conectar(true);  

        }
        
        private static void ConnectionStateOnline(Object sender, StateChangeEventArgs e)
        {
            if (e.CurrentState == ConnectionState.Closed)
            {
                try
                {
                    time.Start();
                }
                catch
                {}
               
            }
            else if (e.CurrentState == ConnectionState.Open)
            {
                StatusOnline = "Conectado";
                if (StatusChanged != null)
                    StatusChanged(null, new StatusEventArgs("Conectado"));
            }
        }


        private static void OnTimedSincEvent(Object sender, EventArgs e)
        {
            Sincronizar();            
        }
        private static void OnTimedEvent(Object sender, EventArgs e)
        {
            TimerRepeated+=1;
            if (Properties.Settings.Default.TempoTentativa/1000 <= TimerRepeated)
            {
                StatusOnline = "Tentando conectar...";
                if (StatusChanged != null)
                    StatusChanged(sender, new StatusEventArgs("Tentando conectar..."));
                online.conectar(true);
                TimerRepeated = 0;
            }
            else
            {          
                StatusOnline = String.Format("Tentando novamente em {0} Segundos.", (Properties.Settings.Default.TempoTentativa / 1000) - TimerRepeated);
                if (StatusChanged != null)
                    StatusChanged(sender, new StatusEventArgs(String.Format("Tentando novamente em {0} segundo.", (Properties.Settings.Default.TempoTentativa/1000)- TimerRepeated)));
                time.Start();
            }
            

        }
       
    }

     public class StatusEventArgs : EventArgs
    {
        public string Status { get; set; }

        public StatusEventArgs(string status)
        {
            Status = status;
        }
    }
        public class Banco
    {
        private  void ConnectionStateChanged(Object sender, StateChangeEventArgs e)
        {
            Console.WriteLine(e.CurrentState.ToString() + "    " + config["type"].ToString());
        }
        public DbConnection conexao = null;
        public Dictionary<string, string> config;
        public Banco(Dictionary<string, string> mConfig)
        {
            config = mConfig;
            switch (types())
            {
                case 1:
                    conexao = new SqlConnection(config["conn"]);
                    break;
                case 2:
                    conexao = new MySqlConnection(config["conn"]);
                    break;
            }
            conexao.StateChange += ConnectionStateChanged;

        }
        public void conectar(Boolean Async = false)
        {
            if (Async)
            {
                conexao.OpenAsync();
            }
            else
            {
                conexao.Open();
            }
            
        }

        private int types()
        {
            if (config["type"].Equals("sql"))
            {
                return 1;
            }
            else if (config["type"].Equals("mysql"))
            {
                return 2;
            }
            else
            {
                throw new System.TypeAccessException("formato não reconhecido");
            }
        }
        public Boolean isConectado
        {
            get
            {
                return conexao.State == ConnectionState.Open;
            }
        }
        public Boolean desconectar()
        {
            if (isConectado)
            {
                try
                {
                    conexao.Close();
                }
                catch
                {
                    return false;
                }

            }
            return true;
        }

        public IDbCommand Command(string cmd = null)
        {
            if (isConectado)
            {
                IDbCommand cmma;
                switch (types())
                {
                    case 1:
                        cmma = new SqlCommand(cmd);
                        break;
                    case 2:
                        cmma = new MySqlCommand(cmd);
                        break;
                    default:
                        return null;
                }
                cmma.Connection = conexao;
                return cmma;

            }
            return null;

        }
    }

}
