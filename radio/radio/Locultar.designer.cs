﻿namespace radio
{
    partial class Locultar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtAvancar = new System.Windows.Forms.Button();
            this.BtPular = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.contAtual = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Cont = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.locutor = new System.Windows.Forms.Label();
            this.Propaganda = new System.Windows.Forms.RichTextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TsStatusBanco = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtAvancar
            // 
            this.BtAvancar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtAvancar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtAvancar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtAvancar.Location = new System.Drawing.Point(623, 12);
            this.BtAvancar.Name = "BtAvancar";
            this.BtAvancar.Size = new System.Drawing.Size(75, 461);
            this.BtAvancar.TabIndex = 0;
            this.BtAvancar.Text = "AVANÇAR";
            this.BtAvancar.UseVisualStyleBackColor = true;
            this.BtAvancar.Click += new System.EventHandler(this.BtAvancar_Click);
            // 
            // BtPular
            // 
            this.BtPular.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BtPular.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtPular.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtPular.Location = new System.Drawing.Point(12, 409);
            this.BtPular.Name = "BtPular";
            this.BtPular.Size = new System.Drawing.Size(605, 64);
            this.BtPular.TabIndex = 2;
            this.BtPular.Text = "PULAR";
            this.BtPular.UseVisualStyleBackColor = true;
            this.BtPular.Click += new System.EventHandler(this.BtPular_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Propaganda x";
            // 
            // contAtual
            // 
            this.contAtual.AutoSize = true;
            this.contAtual.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contAtual.Location = new System.Drawing.Point(122, 9);
            this.contAtual.Name = "contAtual";
            this.contAtual.Size = new System.Drawing.Size(24, 18);
            this.contAtual.TabIndex = 4;
            this.contAtual.Text = "10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(152, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "DE";
            // 
            // Cont
            // 
            this.Cont.AutoSize = true;
            this.Cont.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cont.Location = new System.Drawing.Point(187, 9);
            this.Cont.Name = "Cont";
            this.Cont.Size = new System.Drawing.Size(32, 18);
            this.Cont.TabIndex = 6;
            this.Cont.Text = "100";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(267, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 18);
            this.label5.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 18);
            this.label6.TabIndex = 8;
            this.label6.Text = "Locutor";
            // 
            // locutor
            // 
            this.locutor.AutoSize = true;
            this.locutor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.locutor.Location = new System.Drawing.Point(77, 35);
            this.locutor.Name = "locutor";
            this.locutor.Size = new System.Drawing.Size(21, 18);
            this.locutor.TabIndex = 9;
            this.locutor.Text = "M";
            // 
            // Propaganda
            // 
            this.Propaganda.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Propaganda.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Propaganda.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Propaganda.Location = new System.Drawing.Point(12, 56);
            this.Propaganda.Name = "Propaganda";
            this.Propaganda.ReadOnly = true;
            this.Propaganda.Size = new System.Drawing.Size(605, 347);
            this.Propaganda.TabIndex = 10;
            this.Propaganda.Text = "";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.TsStatusBanco});
            this.statusStrip1.Location = new System.Drawing.Point(0, 475);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(710, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(43, 17);
            this.toolStripStatusLabel1.Text = "Banco:";
            // 
            // TsStatusBanco
            // 
            this.TsStatusBanco.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.TsStatusBanco.LinkColor = System.Drawing.Color.Red;
            this.TsStatusBanco.Name = "TsStatusBanco";
            this.TsStatusBanco.Size = new System.Drawing.Size(42, 17);
            this.TsStatusBanco.Text = "Online";
            // 
            // Locultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 497);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.Propaganda);
            this.Controls.Add(this.locutor);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Cont);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.contAtual);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtPular);
            this.Controls.Add(this.BtAvancar);
            this.Name = "Locultar";
            this.Text = "Locultar";
            this.Load += new System.EventHandler(this.Locultar_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtAvancar;
        private System.Windows.Forms.Button BtPular;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label contAtual;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Cont;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label locutor;
        private System.Windows.Forms.RichTextBox Propaganda;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel TsStatusBanco;
    }
}