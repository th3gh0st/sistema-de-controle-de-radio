﻿using MySql.Data.MySqlClient;
using radio.banco;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace radio
{
    public partial class senha : Form
    {
        int id;
        public senha(int mId)
        {
            id = mId;
            InitializeComponent();
        }

        private void TbSenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            

                if (TbSenha.Text == "")
                {
                    MessageBox.Show("Todos os campos devem ser preenchidos");

                }
                else
                {
                    String sql = "UPDATE usuarios SET Senha = @Senha WHERE usuarios.Id = " + id;

                    var command = Gerenciador.online.Command(sql);
                    Dictionary<string, object> values = new Dictionary<string, object>();

                    values["@Senha"] = TbSenha.Text;

                    foreach (var item in values)
                    {
                        var p = command.CreateParameter();
                        p.ParameterName = item.Key;
                        p.Value = item.Value;
                        command.Parameters.Add(p);
                    }

                    try
                    {
                        command.ExecuteNonQuery();
                        Gerenciador.Sincronizar();
                        MessageBox.Show("Senha Alterada com sucesso com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                    }
                    catch (MySqlException)
                    {

                        MessageBox.Show("Não Foi possivel salvar!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }



            }
        }
    }
}
