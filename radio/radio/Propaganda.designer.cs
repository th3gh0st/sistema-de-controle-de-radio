﻿namespace radio
{
    partial class Propaganda
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TbNome = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TbOBS = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.DTinicio = new System.Windows.Forms.DateTimePicker();
            this.DTfim = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.RbMasculino = new System.Windows.Forms.RadioButton();
            this.RBfeminino = new System.Windows.Forms.RadioButton();
            this.TbCliente = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.DtHoraFim = new System.Windows.Forms.DateTimePicker();
            this.DThoraInicio = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.NduRepetições = new System.Windows.Forms.NumericUpDown();
            this.CbSegundos = new System.Windows.Forms.ComboBox();
            this.RBoutros = new System.Windows.Forms.RadioButton();
            this.TbDetalhes = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TsStatusBanco = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tSPBProcesso = new System.Windows.Forms.ToolStripProgressBar();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.RbFM = new System.Windows.Forms.RadioButton();
            this.RbAM = new System.Windows.Forms.RadioButton();
            this.RbAmbos = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.RBambosLoc = new System.Windows.Forms.RadioButton();
            this.BtSalvarEditar = new System.Windows.Forms.Button();
            this.NduAviso = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.NduRepetições)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NduAviso)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(571, 60);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(569, 212);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Data";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(571, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Cliente:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(570, 186);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Locutor";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(572, 299);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "OBS";
            // 
            // TbNome
            // 
            this.TbNome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbNome.Location = new System.Drawing.Point(638, 58);
            this.TbNome.Margin = new System.Windows.Forms.Padding(2);
            this.TbNome.Name = "TbNome";
            this.TbNome.Size = new System.Drawing.Size(208, 20);
            this.TbNome.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(571, 275);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Aviso";
            // 
            // TbOBS
            // 
            this.TbOBS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbOBS.Location = new System.Drawing.Point(616, 299);
            this.TbOBS.Margin = new System.Windows.Forms.Padding(2);
            this.TbOBS.Multiline = true;
            this.TbOBS.Name = "TbOBS";
            this.TbOBS.Size = new System.Drawing.Size(230, 139);
            this.TbOBS.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(570, 132);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Secundagem";
            // 
            // DTinicio
            // 
            this.DTinicio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DTinicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTinicio.Location = new System.Drawing.Point(638, 208);
            this.DTinicio.Margin = new System.Windows.Forms.Padding(2);
            this.DTinicio.Name = "DTinicio";
            this.DTinicio.Size = new System.Drawing.Size(92, 20);
            this.DTinicio.TabIndex = 18;
            // 
            // DTfim
            // 
            this.DTfim.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DTfim.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTfim.Location = new System.Drawing.Point(750, 208);
            this.DTfim.Margin = new System.Windows.Forms.Padding(2);
            this.DTfim.Name = "DTfim";
            this.DTfim.Size = new System.Drawing.Size(92, 20);
            this.DTfim.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(730, 212);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "à";
            // 
            // RbMasculino
            // 
            this.RbMasculino.AutoSize = true;
            this.RbMasculino.Checked = true;
            this.RbMasculino.Location = new System.Drawing.Point(37, 2);
            this.RbMasculino.Margin = new System.Windows.Forms.Padding(2);
            this.RbMasculino.Name = "RbMasculino";
            this.RbMasculino.Size = new System.Drawing.Size(34, 17);
            this.RbMasculino.TabIndex = 24;
            this.RbMasculino.TabStop = true;
            this.RbMasculino.Text = "M";
            this.RbMasculino.UseVisualStyleBackColor = true;
            // 
            // RBfeminino
            // 
            this.RBfeminino.AutoSize = true;
            this.RBfeminino.Location = new System.Drawing.Point(2, 2);
            this.RBfeminino.Margin = new System.Windows.Forms.Padding(2);
            this.RBfeminino.Name = "RBfeminino";
            this.RBfeminino.Size = new System.Drawing.Size(31, 17);
            this.RBfeminino.TabIndex = 25;
            this.RBfeminino.Text = "F";
            this.RBfeminino.UseVisualStyleBackColor = true;
            // 
            // TbCliente
            // 
            this.TbCliente.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbCliente.Location = new System.Drawing.Point(638, 94);
            this.TbCliente.Margin = new System.Windows.Forms.Padding(2);
            this.TbCliente.Name = "TbCliente";
            this.TbCliente.Size = new System.Drawing.Size(208, 20);
            this.TbCliente.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(730, 243);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "à";
            // 
            // DtHoraFim
            // 
            this.DtHoraFim.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DtHoraFim.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.DtHoraFim.Location = new System.Drawing.Point(750, 239);
            this.DtHoraFim.Margin = new System.Windows.Forms.Padding(2);
            this.DtHoraFim.Name = "DtHoraFim";
            this.DtHoraFim.ShowUpDown = true;
            this.DtHoraFim.Size = new System.Drawing.Size(92, 20);
            this.DtHoraFim.TabIndex = 29;
            this.DtHoraFim.Value = new System.DateTime(1997, 1, 1, 23, 0, 0, 0);
            // 
            // DThoraInicio
            // 
            this.DThoraInicio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DThoraInicio.CustomFormat = "";
            this.DThoraInicio.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.DThoraInicio.Location = new System.Drawing.Point(638, 239);
            this.DThoraInicio.Margin = new System.Windows.Forms.Padding(2);
            this.DThoraInicio.Name = "DThoraInicio";
            this.DThoraInicio.ShowUpDown = true;
            this.DThoraInicio.Size = new System.Drawing.Size(92, 20);
            this.DThoraInicio.TabIndex = 28;
            this.DThoraInicio.Value = new System.DateTime(1997, 1, 1, 0, 0, 0, 0);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(569, 246);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Hora";
            // 
            // NduRepetições
            // 
            this.NduRepetições.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NduRepetições.Location = new System.Drawing.Point(638, 299);
            this.NduRepetições.Name = "NduRepetições";
            this.NduRepetições.Size = new System.Drawing.Size(47, 20);
            this.NduRepetições.TabIndex = 31;
            this.NduRepetições.Visible = false;
            // 
            // CbSegundos
            // 
            this.CbSegundos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CbSegundos.BackColor = System.Drawing.Color.White;
            this.CbSegundos.FormattingEnabled = true;
            this.CbSegundos.Items.AddRange(new object[] {
            "5",
            "15",
            "30",
            "60"});
            this.CbSegundos.Location = new System.Drawing.Point(639, 126);
            this.CbSegundos.Margin = new System.Windows.Forms.Padding(2);
            this.CbSegundos.Name = "CbSegundos";
            this.CbSegundos.Size = new System.Drawing.Size(52, 21);
            this.CbSegundos.TabIndex = 14;
            // 
            // RBoutros
            // 
            this.RBoutros.AutoSize = true;
            this.RBoutros.Location = new System.Drawing.Point(136, 2);
            this.RBoutros.Margin = new System.Windows.Forms.Padding(2);
            this.RBoutros.Name = "RBoutros";
            this.RBoutros.Size = new System.Drawing.Size(56, 17);
            this.RBoutros.TabIndex = 32;
            this.RBoutros.Text = "Outros";
            this.RBoutros.UseVisualStyleBackColor = true;
            // 
            // TbDetalhes
            // 
            this.TbDetalhes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbDetalhes.Location = new System.Drawing.Point(27, 24);
            this.TbDetalhes.Margin = new System.Windows.Forms.Padding(2);
            this.TbDetalhes.Multiline = true;
            this.TbDetalhes.Name = "TbDetalhes";
            this.TbDetalhes.Size = new System.Drawing.Size(440, 403);
            this.TbDetalhes.TabIndex = 0;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.TsStatusBanco,
            this.toolStripStatusLabel2,
            this.tSPBProcesso});
            this.statusStrip1.Location = new System.Drawing.Point(0, 497);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(888, 22);
            this.statusStrip1.TabIndex = 33;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(43, 17);
            this.toolStripStatusLabel1.Text = "Banco:";
            // 
            // TsStatusBanco
            // 
            this.TsStatusBanco.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.TsStatusBanco.LinkColor = System.Drawing.Color.Red;
            this.TsStatusBanco.Name = "TsStatusBanco";
            this.TsStatusBanco.Size = new System.Drawing.Size(42, 17);
            this.TsStatusBanco.Text = "Online";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.toolStripStatusLabel2.LinkColor = System.Drawing.Color.Red;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(13, 17);
            this.toolStripStatusLabel2.Text = "  ";
            // 
            // tSPBProcesso
            // 
            this.tSPBProcesso.Name = "tSPBProcesso";
            this.tSPBProcesso.Size = new System.Drawing.Size(100, 16);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.RbFM);
            this.flowLayoutPanel1.Controls.Add(this.RbAM);
            this.flowLayoutPanel1.Controls.Add(this.RbAmbos);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(639, 154);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(158, 23);
            this.flowLayoutPanel1.TabIndex = 34;
            // 
            // RbFM
            // 
            this.RbFM.AutoSize = true;
            this.RbFM.Checked = true;
            this.RbFM.Location = new System.Drawing.Point(2, 2);
            this.RbFM.Margin = new System.Windows.Forms.Padding(2);
            this.RbFM.Name = "RbFM";
            this.RbFM.Size = new System.Drawing.Size(40, 17);
            this.RbFM.TabIndex = 21;
            this.RbFM.TabStop = true;
            this.RbFM.Text = "FM";
            this.RbFM.UseVisualStyleBackColor = true;
            // 
            // RbAM
            // 
            this.RbAM.AutoSize = true;
            this.RbAM.Location = new System.Drawing.Point(46, 2);
            this.RbAM.Margin = new System.Windows.Forms.Padding(2);
            this.RbAM.Name = "RbAM";
            this.RbAM.Size = new System.Drawing.Size(41, 17);
            this.RbAM.TabIndex = 19;
            this.RbAM.Text = "AM";
            this.RbAM.UseVisualStyleBackColor = true;
            // 
            // RbAmbos
            // 
            this.RbAmbos.AutoSize = true;
            this.RbAmbos.Location = new System.Drawing.Point(91, 2);
            this.RbAmbos.Margin = new System.Windows.Forms.Padding(2);
            this.RbAmbos.Name = "RbAmbos";
            this.RbAmbos.Size = new System.Drawing.Size(57, 17);
            this.RbAmbos.TabIndex = 20;
            this.RbAmbos.Text = "Ambos";
            this.RbAmbos.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(572, 161);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Rádio";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.Controls.Add(this.RBfeminino);
            this.flowLayoutPanel2.Controls.Add(this.RbMasculino);
            this.flowLayoutPanel2.Controls.Add(this.RBambosLoc);
            this.flowLayoutPanel2.Controls.Add(this.RBoutros);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(639, 182);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(203, 24);
            this.flowLayoutPanel2.TabIndex = 35;
            // 
            // RBambosLoc
            // 
            this.RBambosLoc.AutoSize = true;
            this.RBambosLoc.Location = new System.Drawing.Point(75, 2);
            this.RBambosLoc.Margin = new System.Windows.Forms.Padding(2);
            this.RBambosLoc.Name = "RBambosLoc";
            this.RBambosLoc.Size = new System.Drawing.Size(57, 17);
            this.RBambosLoc.TabIndex = 33;
            this.RBambosLoc.Text = "Ambos";
            this.RBambosLoc.UseVisualStyleBackColor = true;
            this.RBambosLoc.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // BtSalvarEditar
            // 
            this.BtSalvarEditar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtSalvarEditar.Location = new System.Drawing.Point(685, 461);
            this.BtSalvarEditar.Name = "BtSalvarEditar";
            this.BtSalvarEditar.Size = new System.Drawing.Size(99, 27);
            this.BtSalvarEditar.TabIndex = 37;
            this.BtSalvarEditar.Text = "Salvar";
            this.BtSalvarEditar.UseVisualStyleBackColor = true;
            this.BtSalvarEditar.Click += new System.EventHandler(this.BtSalvarEditar_Click);
            // 
            // NduAviso
            // 
            this.NduAviso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NduAviso.Location = new System.Drawing.Point(638, 268);
            this.NduAviso.Name = "NduAviso";
            this.NduAviso.Size = new System.Drawing.Size(47, 20);
            this.NduAviso.TabIndex = 38;
            // 
            // Propaganda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(888, 519);
            this.Controls.Add(this.NduAviso);
            this.Controls.Add(this.BtSalvarEditar);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.DtHoraFim);
            this.Controls.Add(this.DThoraInicio);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TbCliente);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.DTfim);
            this.Controls.Add(this.DTinicio);
            this.Controls.Add(this.CbSegundos);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TbOBS);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TbNome);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TbDetalhes);
            this.Controls.Add(this.NduRepetições);
            this.MinimumSize = new System.Drawing.Size(585, 519);
            this.Name = "Propaganda";
            this.Text = "Nova propaganda";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NduRepetições)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NduAviso)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TbNome;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TbOBS;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker DTinicio;
        private System.Windows.Forms.DateTimePicker DTfim;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton RbMasculino;
        private System.Windows.Forms.RadioButton RBfeminino;
        private System.Windows.Forms.TextBox TbCliente;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker DtHoraFim;
        private System.Windows.Forms.DateTimePicker DThoraInicio;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown NduRepetições;
        private System.Windows.Forms.ComboBox CbSegundos;
        private System.Windows.Forms.RadioButton RBoutros;
        private System.Windows.Forms.TextBox TbDetalhes;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel TsStatusBanco;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripProgressBar tSPBProcesso;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.RadioButton RbFM;
        private System.Windows.Forms.RadioButton RbAmbos;
        private System.Windows.Forms.RadioButton RbAM;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button BtSalvarEditar;
        private System.Windows.Forms.RadioButton RBambosLoc;
        private System.Windows.Forms.NumericUpDown NduAviso;
    }
}

