﻿using radio.banco;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace radio
{
    public partial class login : Form
    {
        public login()
        {            
            InitializeComponent();
        }



        private void Logar()
        {

            if (TbUser.Text == "" )
            {
                MessageBox.Show("Usuario não pode ser vázio", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TbUser.Focus();
            }
            else if( TbSenha.Text == "")
            {
                MessageBox.Show("Senha não pode ser vázio", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TbSenha.Focus();
            }
            else
            {
                string Sql = "SELECT Tipo FROM Usuarios WHERE Login LIKE '" + TbUser.Text + "' AND Senha LIKE '"+ TbSenha.Text + "'";
                if (Gerenciador.online.conexao.State == ConnectionState.Open)
                {
                    IDataReader reader = null;
                    using (IDbCommand dbCommand = Gerenciador.online.Command(Sql))
                    {                        
                        try
                        {
                            reader = dbCommand.ExecuteReader();
                            if (reader.Read())
                            {
                                
                                this.Hide();
                                if ((int)reader["Tipo"] == 1)
                                {
                                    reader.Close();
                                    Adm adm = new Adm();
                                    adm.ShowDialog();
                                }
                                else
                                {
                                    reader.Close();
                                    Alocutar lok = new Alocutar();
                                    lok.ShowDialog();
                                }
                                this.Close();
                            }
                            else
                            {
                                reader.Close();
                                MessageBox.Show("Usuario ou senha Incorreto", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                TbUser.Focus();
                            }
                        }
                        catch (MySqlException)
                        {                            
                            if (Gerenciador.online.conexao.State != ConnectionState.Open)
                            {
                                reader.Close();
                                Logar();                                
                            }
                        }
                        
                    }
                }
                else using (IDbCommand dbCommand = Gerenciador.local.Command(Sql))
                {                     
                    var reader = dbCommand.ExecuteReader();
                    if (reader.Read())
                    {
                            this.Hide();
                            if ((int)reader["Tipo"] == 1)
                            {
                                reader.Close();
                                Adm adm = new Adm();
                                adm.ShowDialog();
                            }
                            else
                            {
                                reader.Close();
                                Alocutar lok = new Alocutar();
                                lok.ShowDialog();
                            }
                            this.Close();
                        }
                    else
                    {                           
                        MessageBox.Show("Usuario ou senha Incorreto", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        TbUser.Focus();
                    }


                }
                               
               
            }

        }

        private void login_Load(object sender, EventArgs e)
        {
            this.Focus();
            TsStatusBanco.Text = Gerenciador.StatusOnline;
            Gerenciador.StatusChanged += new EventHandler<StatusEventArgs>(OnStatusEvent);

        }
        private void OnStatusEvent(object sender, StatusEventArgs e)
        {
            TsStatusBanco.Text = e.Status;
        }

        private void BtLogar_Click(object sender, EventArgs e)
        {
            Logar();
        }

        private void TbUser_KeyDown(object sender, KeyEventArgs e)
        {

            if(e.KeyCode == Keys.Enter)
            {
                if (TbUser.Text == "")
                {
                    MessageBox.Show("Usuario não pode ser vázio", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else if (TbSenha.Text == "")
                {
                    TbSenha.Focus();                  
                }
                else
                {
                    Logar();
                }
            }            
        }

        private void TbSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (TbUser.Text == "")
                {
                    TbUser.Focus();
                }
                else if (TbSenha.Text == "")
                {
                    MessageBox.Show("Senha não pode ser vázio", "Login", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    Logar();
                }
               
            }
        }
    }
}