﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace radio
{
    static class Program
    {
        static public Configuration configuration;
        static public ConnectionStringsSection connectionStringsSection;
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            try
            {
                connectionStringsSection = configuration.GetSection("connectionStrings") as ConnectionStringsSection;
                connectionStringsSection.SectionInformation.ForceSave = true;
                Application.Run(new Carregar());
            }
            catch
            {
                MessageBox.Show("Cópia do aplicativo NÃO AUTORIZADO!","Acesso Negado",MessageBoxButtons.OK,MessageBoxIcon.Stop);
            }
            
            
        }
    }
}
