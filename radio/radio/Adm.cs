﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using radio.banco;
namespace radio
{
    delegate void SetTextCallback(string text);
    public partial class Adm : Form
    {
        public Adm()
        {
            InitializeComponent();
        }

        

                

        private void button5_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = DgUser.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                for (int i = 0; i < selectedRowCount; i++)
                {
                    senha se = new senha((int)DgUser.SelectedRows[i].Cells["IdUser"].Value);
                    se.ShowDialog();
                }
            }
                    
        }

        private void Adm_Load(object sender, EventArgs e)
        {
            TsStatusBanco.Text = Gerenciador.StatusOnline;
            Gerenciador.StatusChanged += new EventHandler<StatusEventArgs>(OnStatusEvent);
            popularProp();
            popularUser();
            
            if (Gerenciador.StatusOnline == "Conectado")
            {
                timerPopular.Start();
                timerPopular.Enabled = true;
            }
        }

        private void OnStatusEvent(object sender, StatusEventArgs e)
        {
            if (BtDeletar.InvokeRequired)
                BtDeletar.BeginInvoke((MethodInvoker)delegate {
                    TsStatusBanco.Text = e.Status;
                    if (e.Status != "Conectado")
                    {
                        BtDeletar.Enabled = false;
                        BtDeletarUser.Enabled = false;
                        BtEditar.Enabled = false;
                        BtNovo.Enabled = false;
                        //BtAtualizar.Enabled = false;
                        //BtAtualizarUser.Enabled = false;
                        BtNovoUser.Enabled = false;
                        BtEditarUser.Enabled = false;
                        BtNovaSenha.Enabled = false;
                        timerPopular.Enabled = false;
                        popularUser();
                        popularProp();
                    }
                    else
                    {
                        BtDeletar.Enabled = true;
                        BtDeletarUser.Enabled = true;
                        BtEditar.Enabled = true;
                        BtNovo.Enabled = true;
                        //BtAtualizar.Enabled = true;
                        //BtAtualizarUser.Enabled = true;
                        BtNovoUser.Enabled = true;
                        BtEditarUser.Enabled = true;
                        BtNovaSenha.Enabled = true;
                        timerPopular.Enabled = true;
                        popularUser();
                        popularProp();
                    }
                });
        }

        BindingSource bs = new BindingSource();
        private void popularProp()
        {
            
            if(Gerenciador.online.conexao.State == ConnectionState.Open)
            {
                using (IDbCommand dbCommand = Gerenciador.online.Command("SELECT *  FROM propagandas "))
                {
                    try
                    {
                        var result = dbCommand.ExecuteReader();
                        DgPropaganda.Rows.Clear();
                        int i = 0;
                        while (result.Read())
                        {
                            DgPropaganda.Rows.Add();

                            DgPropaganda.Rows[i].Cells["Id"].Value = Convert.ToInt32(result["Id"]);
                            
                            DgPropaganda.Rows[i].Cells["Propaganda"].Value = result["nome"];

                            DgPropaganda.Rows[i].Cells["Cliente"].Value = result["Cliente"];

                            DgPropaganda.Rows[i].Cells["Data_ini"].Value = Convert.ToDateTime(result["Data_ini"]);

                            DgPropaganda.Rows[i].Cells["Data_fim"].Value = Convert.ToDateTime(result["Data_fim"]);

                            DgPropaganda.Rows[i].Cells["tempo_init"].Value = result["Tempo_ini"];

                            DgPropaganda.Rows[i].Cells["Tempo_fim"].Value = result["Tempo_fim"];

                            if((int )result["Locutor"] == 1)
                            {
                                DgPropaganda.Rows[i].Cells["Locutor"].Value = "Felinino";
                            }
                            else if ((int)result["Locutor"] == 2)
                            {
                                DgPropaganda.Rows[i].Cells["Locutor"].Value = "Masculino";
                            }
                            else if ((int)result["Locutor"] == 3)
                            {
                                DgPropaganda.Rows[i].Cells["Locutor"].Value = "Ambos";
                            }
                            else{
                                DgPropaganda.Rows[i].Cells["Locutor"].Value = "Outro";
                            }

                            DgPropaganda.Rows[i].Cells["Secundagem"].Value = result["Secundagem"];

                            DgPropaganda.Rows[i].Cells["Vezes_Ar"].Value = result["Vezes_Ar"];

                            bs.DataSource = DgPropaganda.DataSource;
                            i += 1;
                        }
                        result.Close();
                        Gerenciador.Sincronizar();
                    }
                    catch (MySqlException)
                    {
                        if (Gerenciador.online.conexao.State != ConnectionState.Open)
                        {                            
                            popularProp();
                        }
                    }
                }
            }
            else using (IDbCommand dbCommand = Gerenciador.local.Command("SELECT *  FROM propagandas"))
            {
                    var result = dbCommand.ExecuteReader();
                    DgPropaganda.Rows.Clear();
                    int i = 0;
                    while (result.Read())
                    {
                        DgPropaganda.Rows.Add();

                        DgPropaganda.Rows[i].Cells["Id"].Value = Convert.ToInt32(result["Id"]);

                        DgPropaganda.Rows[i].Cells["Propaganda"].Value = result["nome"];

                        DgPropaganda.Rows[i].Cells["Cliente"].Value = result["Cliente"];

                        DgPropaganda.Rows[i].Cells["Data_ini"].Value = Convert.ToDateTime(result["Data_ini"]);

                        DgPropaganda.Rows[i].Cells["Data_fim"].Value = Convert.ToDateTime(result["Data_fim"]);

                        DgPropaganda.Rows[i].Cells["tempo_init"].Value = result["Tempo_ini"];

                        DgPropaganda.Rows[i].Cells["Tempo_fim"].Value = result["Tempo_fim"];

                        if ((int)result["Locutor"] == 1)
                        {
                            DgPropaganda.Rows[i].Cells["Locutor"].Value = "Felinino";
                        }
                        else if ((int)result["Locutor"] == 2)
                        {
                            DgPropaganda.Rows[i].Cells["Locutor"].Value = "Masculino";
                        }
                        else if ((int)result["Locutor"] == 3)
                        {
                            DgPropaganda.Rows[i].Cells["Locutor"].Value = "Ambos";
                        }
                        else
                        {
                            DgPropaganda.Rows[i].Cells["Locutor"].Value = "Outro";
                        }

                        DgPropaganda.Rows[i].Cells["Secundagem"].Value = result["Secundagem"];

                        DgPropaganda.Rows[i].Cells["Vezes_Ar"].Value = result["Vezes_Ar"];

                        bs.DataSource = DgPropaganda.DataSource;
                        i += 1;
                    }
                    result.Close();
                }

        }

        private void popularUser()
        {

            if (Gerenciador.online.conexao.State == ConnectionState.Open)
            {
                using (IDbCommand dbCommand = Gerenciador.online.Command("SELECT usuarios.Id, usuarios.Login,usuarios.Email,tu.TipoUsu FROM usuarios INNER JOIN tipousu tu on tu.Id = usuarios.Tipo"))
                {
                    try
                    {
                        var result = dbCommand.ExecuteReader();
                        DgUser.Rows.Clear();
                        int i = 0;
                        while (result.Read())
                        {
                            DgUser.Rows.Add();
                            DgUser.Rows[i].Cells["IdUser"].Value = Convert.ToInt32(result["Id"]);

                            DgUser.Rows[i].Cells["Nome"].Value = Convert.ToString(result["Login"]);

                            DgUser.Rows[i].Cells["Email"].Value = Convert.ToString(result["Email"]);

                            DgUser.Rows[i].Cells["Cargo"].Value = Convert.ToString(result["TipoUsu"]);
                            i += 1;
                        }
                        result.Close();
                        Gerenciador.Sincronizar();
                    }
                    catch (MySqlException)
                    {
                        if (Gerenciador.online.conexao.State != ConnectionState.Open)
                        {
                            popularUser();
                        }
                    }
                }
            }
            else using (IDbCommand dbCommand = Gerenciador.local.Command("SELECT usuarios.Id, usuarios.Login,usuarios.Email,tu.TipoUsu FROM usuarios INNER JOIN tipousu tu on tu.Id = usuarios.Tipo"))
                {
                    var result = dbCommand.ExecuteReader();
                    DgUser.Rows.Clear();
                    int i = 0;
                    while (result.Read())
                    {
                        DgUser.Rows.Add();
                        DgUser.Rows[i].Cells["IdUser"].Value = Convert.ToInt32(result["Id"]);

                        DgUser.Rows[i].Cells["Nome"].Value = Convert.ToString(result["Login"]);

                        DgUser.Rows[i].Cells["Email"].Value = Convert.ToString(result["Email"]);

                        DgUser.Rows[i].Cells["Cargo"].Value = Convert.ToString(result["TipoUsu"]);
                        i += 1;
                    }
                    result.Close();
                }

        }

        private Boolean deletarProp(int id)
        {
            if (Gerenciador.online.conexao.State == ConnectionState.Open)
            {
                try
                {
                    Gerenciador.online.Command(String.Format("DELETE FROM propagandas WHERE propagandas.Id = {0}", id)).ExecuteNonQuery();
                    popularProp();
                    return true;
                }
                catch (MySqlException)
                {
                    
                    return false;
                    
                }
            }
            else
            {
                return false;
            }
            
        }

        private Boolean deletarUser(int id)
        {
            if (Gerenciador.online.conexao.State == ConnectionState.Open)
            {
                try
                {
                    Gerenciador.online.Command(String.Format("DELETE FROM usuarios WHERE usuarios.Id = {0}", id)).ExecuteNonQuery();
                    popularUser();
                    return true;
                }
                catch (MySqlException)
                {

                    return false;

                }
            }
            else
            {
                return false;
            }
            

        }
                
        private void button3_Click(object sender, EventArgs e)
        {
            Program.connectionStringsSection.ConnectionStrings["BancoOn"].ConnectionString = tBconnection.Text;
            Program.configuration.Save();

            tBconnection.Text = null;
            MessageBox.Show("ConnectionString alterado com sucesso.");
            Program.configuration.Save();
        }

        private void BtDeletarUser_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = DgUser.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                for (int i = 0; i < selectedRowCount; i++)
                {
                    DialogResult delete = MessageBox.Show(String.Format("Você quer mesmo deletar o usuario {0}? ", DgUser.SelectedRows[i].Cells["Nome"].Value), "Aviso", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    switch (delete)
                    {
                        case (DialogResult.Yes):
                            if (!deletarUser((int)DgUser.SelectedRows[i].Cells["IdUser"].Value))
                            {
                                MessageBox.Show(String.Format("Não Foi possivel apagar o usuario {0}.", DgUser.SelectedRows[i].Cells["Nome"].Value), "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            }
                            break;
                        case (DialogResult.Cancel):
                            i = selectedRowCount;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void DgPropaganda_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (TsStatusBanco.Text != "Conectado")
            {
                e.Cancel = true;
            }
            else { 
                DialogResult delete = MessageBox.Show(String.Format("Você quer mesmo deletar a propaganda {0}? ", e.Row.Cells["Propaganda"].Value), "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (delete)
                {
                    case (DialogResult.Yes):
                        if (!deletarProp((int)e.Row.Cells["Id"].Value)) {
                            e.Cancel = true;
                            MessageBox.Show(String.Format("Não Foi possivel apagar a propaganda {0}. ", e.Row.Cells["Propaganda"].Value), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        break;
                    default:
                        e.Cancel = true;
                        break;
                }
            }
        }

        private void DgUser_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {

            if (TsStatusBanco.Text != "Conectado")
            {
                e.Cancel = true;
            }
            else
            {
                DialogResult delete = MessageBox.Show(String.Format("Você quer mesmo deletar o usuario {0}? ", e.Row.Cells["Nome"].Value), "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                switch (delete)
                {
                    case (DialogResult.Yes):
                        if (!deletarUser((int)e.Row.Cells["IdUser"].Value))
                        {
                            e.Cancel = true;
                            MessageBox.Show(String.Format("Não Foi possivel apagar o usuario {0}. ", e.Row.Cells["Nome"].Value), "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        break;
                    default:
                        e.Cancel = true;
                        break;
                }
            }
        }

        private void BtAtualizarUser_Click(object sender, EventArgs e)
        {
            popularUser();
        }

        private void timerPopular_Tick(object sender, EventArgs e)
        {
            popularUser();
            popularProp();            
        }

        private void BtEditarUser_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = DgUser.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                for (int i = 0; i < selectedRowCount; i++)
                {
                    Usuario pop = new Usuario(1, (int)DgUser.SelectedRows[i].Cells["IdUser"].Value);
                    pop.Show();
                }
            }
        }

        private void BtNovo_Click(object sender, EventArgs e)
        {
            Propaganda pop = new Propaganda(0);
            pop.ShowDialog();
            popularProp();

        }

        private void BtEditar_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = DgPropaganda.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                for (int i = 0; i < selectedRowCount; i++)
                {
                    Propaganda pop = new Propaganda(1, (int)DgPropaganda.SelectedRows[i].Cells["Id"].Value);
                    pop.Show();                    
                }
            }
        }

        private void BtVisualizar_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = DgPropaganda.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                for (int i = 0; i < selectedRowCount; i++)
                {
                    Propaganda pop = new Propaganda(2, (int)DgPropaganda.SelectedRows[i].Cells["Id"].Value);
                    pop.Show();
                }
            }
        }

        private void BtDeletar_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = DgPropaganda.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                for (int i = 0; i < selectedRowCount; i++)
                {
                    DialogResult delete = MessageBox.Show(String.Format("Você quer mesmo deletar a propaganda {0}? ", DgPropaganda.SelectedRows[i].Cells["Propaganda"].Value), "Aviso", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    switch (delete)
                    {
                        case (DialogResult.Yes):
                            if (!deletarProp((int)DgPropaganda.SelectedRows[i].Cells["Id"].Value))
                            {
                                MessageBox.Show(String.Format("Não Foi possivel apagar a propaganda {0}.", DgPropaganda.SelectedRows[i].Cells["Propaganda"].Value), "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            }
                            popularProp();

                            break;
                        case (DialogResult.Cancel):
                            i = selectedRowCount;
                            break;
                        default:
                            break;
                    }


                }
            }
        }

        private void BtAtualizar_Click(object sender, EventArgs e)
        {
            popularProp();
        }

        private void DgPropaganda_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Propaganda pop = new Propaganda(2, (int)DgPropaganda.Rows[e.RowIndex].Cells["Id"].Value);
            pop.Show();
        }
        Boolean selec = false;
        private void DgPropaganda_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            
            if (e.StateChanged == DataGridViewElementStates.Selected)
            {
                selec = !selec;
            }
            if (TsStatusBanco.Text == "Conectado")
            {
                BtDeletar.Enabled = selec;
                BtEditar.Enabled = selec;

            }                
            BtVisualizar.Enabled = selec;

        }

        private void BtNovoUser_Click(object sender, EventArgs e)
        {
            Usuario user = new Usuario(0);
            user.ShowDialog();
            popularUser();
        }

        private void DgUser_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Usuario user = new Usuario(2, (int)DgUser.Rows[e.RowIndex].Cells["IdUser"].Value);
            user.ShowDialog();
            popularUser();
        }
        Boolean selecUser = false;
        private void DgUser_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (e.StateChanged == DataGridViewElementStates.Selected)
            {
                selecUser = !selecUser;
            }
            if (TsStatusBanco.Text == "Conectado")
            {
                BtDeletarUser.Enabled = selecUser;
                BtEditarUser.Enabled = selecUser;
                BtNovaSenha.Enabled = selecUser;
            }
            BtVisualizarUser.Enabled = selecUser;
        }

        private void BuscarUsuario_Click(object sender, EventArgs e)
        {

            

        }

        private void Buscar_Click(object sender, EventArgs e)
        {
            bs.DataSource = DgPropaganda.DataSource;
            bs.Filter = string.Format(" Propaganda = 'Teste'", BuscaPropanda.Text);
            DgPropaganda.DataSource = bs;
        }

        private void BtVisualizarUser_Click(object sender, EventArgs e)
        {
            Int32 selectedRowCount = DgUser.Rows.GetRowCount(DataGridViewElementStates.Selected);
            if (selectedRowCount > 0)
            {
                for (int i = 0; i < selectedRowCount; i++)
                {
                    Usuario user = new Usuario(2, (int)DgUser.SelectedRows[i].Cells["IdUser"].Value);
                    user.Show();
                }
            }
        }

        private void DgUser_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
