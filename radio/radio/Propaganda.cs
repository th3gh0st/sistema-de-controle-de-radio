﻿using MySql.Data.MySqlClient;
using radio.banco;
using radio.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace radio
{
    public partial class Propaganda : Form
    {
        int type;
        int id;
        public Propaganda(int mType = 0, int mId = 0)
        {
            type = mType;
            id = mId;

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TsStatusBanco.Text = Gerenciador.StatusOnline;
            Gerenciador.StatusChanged += new EventHandler<StatusEventArgs>(OnStatusEvent);

            if (type == 1)
            {
                popularProp();
                this.Text = "Editando " + TbNome.Text;

            }
            else if (type == 2)
            {
                popularProp();
                alternarBloqueio();
                BtSalvarEditar.Text = "&Editar";
                this.Text = "Visualizar " + TbNome.Text;
            }             
        }

        private void popularProp()
        {
            if (Gerenciador.online.conexao.State == ConnectionState.Open)
            {
                using (IDbCommand dbCommand = Gerenciador.online.Command("SELECT *  FROM propagandas WHERE id = "+ id))
                {
                    try
                    {
                        var result = dbCommand.ExecuteReader();
                        while (result.Read())
                        {

                            TbDetalhes.Text = Convert.ToString(result["Propaganda"]);
                           
                            TbNome.Text = Convert.ToString(result["nome"]);
                            TbCliente.Text = Convert.ToString(result["Cliente"]);
                            TbOBS.Text = Convert.ToString(result["Observacoes"]);

                            switch ((int)result["Secundagem"])
                            {
                                case 5:
                                    CbSegundos.SelectedIndex= 0;
                                    break;
                                case 15:
                                    CbSegundos.SelectedIndex = 1;
                                    break;
                                case 30:
                                    CbSegundos.SelectedIndex = 2;
                                    break;
                                case 60:
                                    CbSegundos.SelectedIndex = 3;
                                    break;
                                default:
                                    CbSegundos.SelectedIndex = -1;
                                    break;

                            }

                            if ((int)result["Radio"] == 1)
                            {
                                RbFM.Checked = true;
                            }
                            else if ((int)result["Radio"] == 2)
                            {
                                RbAM.Checked = true;
                            }
                            else if ((int)result["Radio"] == 3)
                            {
                                RbAmbos.Checked = true;
                            }

                            if ((int)result["Locutor"] == 1)
                            {
                                RBfeminino.Checked = true;
                            }
                            else if ((int)result["Locutor"] == 2)
                            {
                                RbMasculino.Checked = true;
                            }
                            else if ((int)result["Locutor"] == 3)
                            {
                                RBambosLoc.Checked = true;
                            }
                            else
                            {
                                RBoutros.Checked = true;
                            }

                            DTinicio.Value = Convert.ToDateTime(result["Data_ini"]);

                            DTfim.Value = Convert.ToDateTime(result["Data_fim"]);

                            DThoraInicio.Value = new DateTime(1997, 01, 01)+(TimeSpan) result["Tempo_ini"];

                            DtHoraFim.Value = new DateTime(1997, 01, 01) + (TimeSpan)result["Tempo_fim"];

                            NduRepetições.Value = (int)result["Repetir"];

                            NduAviso.Value = (int)result["Aviso"];
                        }
                        result.Close();
                        Gerenciador.Sincronizar();
                    }
                    catch (MySqlException)
                    {
                        if (Gerenciador.online.conexao.State != ConnectionState.Open)
                        {
                            popularProp();
                        }
                    }
                }
            }
            else using (IDbCommand dbCommand = Gerenciador.local.Command("SELECT *  FROM propagandas"))
                {
                    dbCommand.Dispose();
                    var result = dbCommand.ExecuteReader();
                    while (result.Read())
                    {

                        TbDetalhes.Text = Convert.ToString(result["Propaganda"]);

                        TbNome.Text = Convert.ToString(result["nome"]);
                        TbCliente.Text = Convert.ToString(result["Cliente"]);
                        TbOBS.Text = Convert.ToString(result["Observacoes"]);

                        switch ((int)result["Secundagem"])
                        {
                            case 5:
                                CbSegundos.SelectedIndex = 0;
                                break;
                            case 15:
                                CbSegundos.SelectedIndex = 1;
                                break;
                            case 30:
                                CbSegundos.SelectedIndex = 2;
                                break;
                            case 60:
                                CbSegundos.SelectedIndex = 3;
                                break;
                            default:
                                CbSegundos.SelectedIndex = -1;
                                break;

                        }

                        if ((int)result["Radio"] == 1)
                        {
                            RbFM.Checked = true;
                        }
                        else if ((int)result["Radio"] == 2)
                        {
                            RbAM.Checked = true;
                        }
                        else if ((int)result["Radio"] == 3)
                        {
                            RbAmbos.Checked = true;
                        }

                        if ((int)result["Locutor"] == 1)
                        {
                            RBfeminino.Checked = true;
                        }
                        else if ((int)result["Locutor"] == 2)
                        {
                            RbMasculino.Checked = true;
                        }
                        else if ((int)result["Locutor"] == 3)
                        {
                            RBambosLoc.Checked = true;
                        }
                        else
                        {
                            RBoutros.Checked = true;
                        }

                        DTinicio.Value = Convert.ToDateTime(result["Data_ini"]);

                        DTfim.Value = Convert.ToDateTime(result["Data_fim"]);

                        DThoraInicio.Value = new DateTime(1997, 01, 01) + (TimeSpan)result["Tempo_ini"];

                        DtHoraFim.Value = new DateTime(1997, 01, 01) + (TimeSpan)result["Tempo_fim"];

                        NduRepetições.Value = (int)result["Repetir"];

                        NduAviso.Value = (int)result["Aviso"];
                    }
                    result.Close();
                }
        }


        private void OnStatusEvent(object sender, StatusEventArgs e)
        {
            if (BtSalvarEditar.InvokeRequired)
                BtSalvarEditar.BeginInvoke((MethodInvoker)delegate {
                    TsStatusBanco.Text = e.Status;
                    if (e.Status != "Conectado")
                    {
                        BtSalvarEditar.Enabled = false;
                    }
                    else
                    {
                        BtSalvarEditar.Enabled = true;
                    }
                });
        }

        private void alternarBloqueio()
        {
            TbCliente.ReadOnly = !TbCliente.ReadOnly;
            TbDetalhes.ReadOnly = !TbDetalhes.ReadOnly;
            TbNome.ReadOnly = !TbNome.ReadOnly;
            TbOBS.ReadOnly = !TbOBS.ReadOnly;
            CbSegundos.Enabled = !CbSegundos.Enabled;
            RbAM.Enabled = !RbAM.Enabled;
            RbAmbos.Enabled = !RbAmbos.Enabled;
            NduAviso.ReadOnly = !NduAviso.ReadOnly;
            RBfeminino.Enabled = !RBfeminino.Enabled;
            RbFM.Enabled = !RbFM.Enabled;
            RbMasculino.Enabled = !RbMasculino.Enabled;
            RBambosLoc.Enabled = !RBambosLoc.Enabled;
            RBoutros.Enabled = !RBoutros.Enabled;
            DTfim.Enabled = !DTfim.Enabled;
            DtHoraFim.Enabled = !DtHoraFim.Enabled;
            DTinicio.Enabled = !DTinicio.Enabled;
            DThoraInicio.Enabled = !DThoraInicio.Enabled;
            NduRepetições.ReadOnly = !NduRepetições.ReadOnly;
        }

        private void BtSalvarEditar_Click(object sender, EventArgs e)
        {
            if (type == 2 && TbCliente.ReadOnly){
                alternarBloqueio();
                this.Text = "Editando " + TbNome.Text;
                BtSalvarEditar.Text = "&Salvar";
            }
            else
            {

                if(TbCliente.Text == "" || 
                   TbDetalhes.Text == ""||
                   TbNome.Text == ""||
                   (string)CbSegundos.SelectedItem is null ||
                   (!RbFM.Checked && !RbAM.Checked && !RbAmbos.Checked))
                {
                    MessageBox.Show("Todos os campos devem ser preenchidos");

                }
                else
                {
                    String sql = "";
                    if (type == 0)
                    {
                         sql = "INSERT INTO propagandas (Id, Propaganda, Cliente, Radio, Repetir, Data_ini, Data_fim, Vezes_Ar, Secundagem, Tempo_ini, Tempo_fim, nome, Locutor, Aviso, Observacoes) VALUES " +
                            "(NULL, @Propaganda, @Cliente, @Radio, @Repetir, @Data_ini, @Data_fim, 0, @Secundagem, @Tempo_ini, @Tempo_fim, @nome, @Locutor, @Aviso, @obs);";

                    }
                    else
                    {
                        sql = " UPDATE propagandas SET Propaganda = @Propaganda, Cliente = @Cliente, Radio = @Radio, Repetir = @Repetir, Data_ini = @Data_ini, Data_fim = @Data_fim, Secundagem = @Secundagem, Tempo_ini = @Tempo_ini, Tempo_fim = @Tempo_fim , nome = @nome, Locutor = @Locutor, Aviso = @Aviso, Observacoes = @obs WHERE Id = "+ id;
                    }
                    var command = Gerenciador.online.Command(sql);
                    Dictionary<string, object> values = new Dictionary<string, object>();

                    values["@Propaganda"] = TbDetalhes.Text;
                    values["@Cliente"] = TbCliente.Text;
                    values["@nome"] = TbNome.Text;
                    values["@obs"] = TbOBS.Text;

                    if (RbFM.Checked)
                    {
                        values["@Radio"] = 1;
                    }
                    else if (RbAM.Checked)
                    {
                        values["@Radio"] = 2;
                    }
                    else 
                    {
                        values["@Radio"] = 3;
                    }
                    values["@Repetir"] = NduRepetições.Value;
                    values["@Aviso"] = NduAviso.Value;

                    values["@Data_ini"] = DTinicio.Value;
                    values["@Data_fim"] = DTfim.Value;                 
                    
                    values["@Tempo_ini"] = DThoraInicio.Value.TimeOfDay;
                    values["@Tempo_fim"] = DtHoraFim.Value.TimeOfDay;

                    values["@Secundagem"] = (String)CbSegundos.SelectedItem;

                    if (RBfeminino.Checked)
                    {
                        values["@Locutor"] = 1;
                    }
                    else if (RbMasculino.Checked)
                    {
                        values["@Locutor"] = 2;
                    }
                    else if (RBambosLoc.Checked)
                    {
                        values["@Locutor"] = 3;
                    }
                    else
                    {
                        values["@Locutor"] = 4;
                    }                   

                    foreach (var item in values)
                    {
                        var p = command.CreateParameter();
                        p.ParameterName = item.Key;
                        p.Value = item.Value;
                        command.Parameters.Add(p);
                    }
                    tSPBProcesso.Style = ProgressBarStyle.Marquee;
                    try
                    {
                        var t = Task.Run(async delegate
                        {
                            await Task.Delay(1000);
                            return 42;
                        });
                        t.Wait();
                        command.ExecuteNonQuery();
                        MessageBox.Show("Salvo com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    catch (MySqlException)
                    {
                        
                        MessageBox.Show("Não Foi possivel salvar!","Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    //tSPBProcesso.Style = ProgressBarStyle.Blocks;
                    //tSPBProcesso.Value = 100;


                }
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }

   
}
