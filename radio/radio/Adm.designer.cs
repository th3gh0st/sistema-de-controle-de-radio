﻿namespace radio
{
    partial class Adm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.DgUser = new System.Windows.Forms.DataGridView();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cargo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtAtualizarUser = new System.Windows.Forms.Button();
            this.BtNovaSenha = new System.Windows.Forms.Button();
            this.BuscarEmail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.BtDeletarUser = new System.Windows.Forms.Button();
            this.BtVisualizarUser = new System.Windows.Forms.Button();
            this.BuscarCargo = new System.Windows.Forms.ComboBox();
            this.BtEditarUser = new System.Windows.Forms.Button();
            this.BtNovoUser = new System.Windows.Forms.Button();
            this.BuscarUsuario = new System.Windows.Forms.Button();
            this.BuscarNome = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TpPropagandas = new System.Windows.Forms.TabPage();
            this.DgPropaganda = new System.Windows.Forms.DataGridView();
            this.Propaganda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vezes_Ar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data_ini = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data_fim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tempo_init = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tempo_fim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Locutor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Secundagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BtAtualizar = new System.Windows.Forms.Button();
            this.BtDeletar = new System.Windows.Forms.Button();
            this.BtVisualizar = new System.Windows.Forms.Button();
            this.BtEditar = new System.Windows.Forms.Button();
            this.BtNovo = new System.Windows.Forms.Button();
            this.Buscar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.BuscaStatus = new System.Windows.Forms.ComboBox();
            this.BuscarCliente = new System.Windows.Forms.TextBox();
            this.BuscaPropanda = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tBconnection = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.BtMudarConn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.NuTentativas = new System.Windows.Forms.NumericUpDown();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.TsStatusBanco = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerPopular = new System.Windows.Forms.Timer(this.components);
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgUser)).BeginInit();
            this.TpPropagandas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgPropaganda)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NuTentativas)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.DgUser);
            this.tabPage1.Controls.Add(this.BtAtualizarUser);
            this.tabPage1.Controls.Add(this.BtNovaSenha);
            this.tabPage1.Controls.Add(this.BuscarEmail);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.BtDeletarUser);
            this.tabPage1.Controls.Add(this.BtVisualizarUser);
            this.tabPage1.Controls.Add(this.BuscarCargo);
            this.tabPage1.Controls.Add(this.BtEditarUser);
            this.tabPage1.Controls.Add(this.BtNovoUser);
            this.tabPage1.Controls.Add(this.BuscarUsuario);
            this.tabPage1.Controls.Add(this.BuscarNome);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(918, 400);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Usuarios";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // DgUser
            // 
            this.DgUser.AllowUserToAddRows = false;
            this.DgUser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DgUser.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.DgUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgUser.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.Email,
            this.Cargo,
            this.IdUser});
            this.DgUser.Location = new System.Drawing.Point(8, 3);
            this.DgUser.Name = "DgUser";
            this.DgUser.ReadOnly = true;
            this.DgUser.Size = new System.Drawing.Size(771, 375);
            this.DgUser.TabIndex = 0;
            this.DgUser.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgUser_CellContentClick);
            this.DgUser.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgUser_CellContentDoubleClick);
            this.DgUser.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.DgUser_RowStateChanged);
            this.DgUser.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.DgUser_UserDeletingRow);
            // 
            // Nome
            // 
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            this.Nome.Width = 60;
            // 
            // Email
            // 
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            this.Email.Width = 57;
            // 
            // Cargo
            // 
            this.Cargo.HeaderText = "Cargo";
            this.Cargo.Name = "Cargo";
            this.Cargo.ReadOnly = true;
            this.Cargo.Width = 60;
            // 
            // IdUser
            // 
            this.IdUser.HeaderText = "IdUser";
            this.IdUser.Name = "IdUser";
            this.IdUser.ReadOnly = true;
            this.IdUser.Visible = false;
            this.IdUser.Width = 63;
            // 
            // BtAtualizarUser
            // 
            this.BtAtualizarUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtAtualizarUser.Location = new System.Drawing.Point(813, 221);
            this.BtAtualizarUser.Name = "BtAtualizarUser";
            this.BtAtualizarUser.Size = new System.Drawing.Size(75, 23);
            this.BtAtualizarUser.TabIndex = 20;
            this.BtAtualizarUser.Text = "&Atualizar";
            this.BtAtualizarUser.UseVisualStyleBackColor = true;
            this.BtAtualizarUser.Click += new System.EventHandler(this.BtAtualizarUser_Click);
            // 
            // BtNovaSenha
            // 
            this.BtNovaSenha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtNovaSenha.Location = new System.Drawing.Point(813, 264);
            this.BtNovaSenha.Name = "BtNovaSenha";
            this.BtNovaSenha.Size = new System.Drawing.Size(75, 23);
            this.BtNovaSenha.TabIndex = 19;
            this.BtNovaSenha.Text = "Nova Senha";
            this.BtNovaSenha.UseVisualStyleBackColor = true;
            this.BtNovaSenha.Click += new System.EventHandler(this.button5_Click);
            // 
            // BuscarEmail
            // 
            this.BuscarEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuscarEmail.Location = new System.Drawing.Point(287, 22);
            this.BuscarEmail.Name = "BuscarEmail";
            this.BuscarEmail.Size = new System.Drawing.Size(251, 20);
            this.BuscarEmail.TabIndex = 18;
            this.BuscarEmail.Visible = false;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(290, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Email";
            this.label4.Visible = false;
            // 
            // BtDeletarUser
            // 
            this.BtDeletarUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtDeletarUser.Location = new System.Drawing.Point(813, 176);
            this.BtDeletarUser.Name = "BtDeletarUser";
            this.BtDeletarUser.Size = new System.Drawing.Size(75, 23);
            this.BtDeletarUser.TabIndex = 16;
            this.BtDeletarUser.Text = "&Deletar";
            this.BtDeletarUser.UseVisualStyleBackColor = true;
            this.BtDeletarUser.Click += new System.EventHandler(this.BtDeletarUser_Click);
            // 
            // BtVisualizarUser
            // 
            this.BtVisualizarUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtVisualizarUser.Location = new System.Drawing.Point(813, 134);
            this.BtVisualizarUser.Name = "BtVisualizarUser";
            this.BtVisualizarUser.Size = new System.Drawing.Size(75, 23);
            this.BtVisualizarUser.TabIndex = 13;
            this.BtVisualizarUser.Text = "&Visualizar";
            this.BtVisualizarUser.UseVisualStyleBackColor = true;
            this.BtVisualizarUser.Click += new System.EventHandler(this.BtVisualizarUser_Click);
            // 
            // BuscarCargo
            // 
            this.BuscarCargo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuscarCargo.FormattingEnabled = true;
            this.BuscarCargo.Items.AddRange(new object[] {
            "Administrador",
            "Locutor"});
            this.BuscarCargo.Location = new System.Drawing.Point(544, 21);
            this.BuscarCargo.Name = "BuscarCargo";
            this.BuscarCargo.Size = new System.Drawing.Size(156, 21);
            this.BuscarCargo.TabIndex = 12;
            this.BuscarCargo.Visible = false;
            // 
            // BtEditarUser
            // 
            this.BtEditarUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtEditarUser.Location = new System.Drawing.Point(813, 91);
            this.BtEditarUser.Name = "BtEditarUser";
            this.BtEditarUser.Size = new System.Drawing.Size(75, 23);
            this.BtEditarUser.TabIndex = 11;
            this.BtEditarUser.Text = "&Editar";
            this.BtEditarUser.UseVisualStyleBackColor = true;
            this.BtEditarUser.Click += new System.EventHandler(this.BtEditarUser_Click);
            // 
            // BtNovoUser
            // 
            this.BtNovoUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtNovoUser.Location = new System.Drawing.Point(813, 47);
            this.BtNovoUser.Name = "BtNovoUser";
            this.BtNovoUser.Size = new System.Drawing.Size(75, 23);
            this.BtNovoUser.TabIndex = 10;
            this.BtNovoUser.Text = "&Novo";
            this.BtNovoUser.UseVisualStyleBackColor = true;
            this.BtNovoUser.Click += new System.EventHandler(this.BtNovoUser_Click);
            // 
            // BuscarUsuario
            // 
            this.BuscarUsuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuscarUsuario.Location = new System.Drawing.Point(704, 19);
            this.BuscarUsuario.Name = "BuscarUsuario";
            this.BuscarUsuario.Size = new System.Drawing.Size(75, 23);
            this.BuscarUsuario.TabIndex = 9;
            this.BuscarUsuario.Text = "&Buscar";
            this.BuscarUsuario.UseVisualStyleBackColor = true;
            this.BuscarUsuario.Visible = false;
            this.BuscarUsuario.Click += new System.EventHandler(this.BuscarUsuario_Click);
            // 
            // BuscarNome
            // 
            this.BuscarNome.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuscarNome.Location = new System.Drawing.Point(8, 22);
            this.BuscarNome.Name = "BuscarNome";
            this.BuscarNome.Size = new System.Drawing.Size(273, 20);
            this.BuscarNome.TabIndex = 2;
            this.BuscarNome.Visible = false;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(547, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Cargo";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Nome";
            this.label6.Visible = false;
            // 
            // TpPropagandas
            // 
            this.TpPropagandas.Controls.Add(this.DgPropaganda);
            this.TpPropagandas.Controls.Add(this.BtAtualizar);
            this.TpPropagandas.Controls.Add(this.BtDeletar);
            this.TpPropagandas.Controls.Add(this.BtVisualizar);
            this.TpPropagandas.Controls.Add(this.BtEditar);
            this.TpPropagandas.Controls.Add(this.BtNovo);
            this.TpPropagandas.Controls.Add(this.Buscar);
            this.TpPropagandas.Controls.Add(this.label3);
            this.TpPropagandas.Controls.Add(this.BuscaStatus);
            this.TpPropagandas.Controls.Add(this.BuscarCliente);
            this.TpPropagandas.Controls.Add(this.BuscaPropanda);
            this.TpPropagandas.Controls.Add(this.label2);
            this.TpPropagandas.Controls.Add(this.label1);
            this.TpPropagandas.Location = new System.Drawing.Point(4, 22);
            this.TpPropagandas.Name = "TpPropagandas";
            this.TpPropagandas.Padding = new System.Windows.Forms.Padding(3);
            this.TpPropagandas.Size = new System.Drawing.Size(918, 400);
            this.TpPropagandas.TabIndex = 0;
            this.TpPropagandas.Text = "Propagandas";
            this.TpPropagandas.UseVisualStyleBackColor = true;
            // 
            // DgPropaganda
            // 
            this.DgPropaganda.AllowUserToAddRows = false;
            this.DgPropaganda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DgPropaganda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.DgPropaganda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgPropaganda.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Propaganda,
            this.Id,
            this.Vezes_Ar,
            this.Cliente,
            this.Data_ini,
            this.Data_fim,
            this.tempo_init,
            this.Tempo_fim,
            this.Locutor,
            this.Secundagem});
            this.DgPropaganda.Location = new System.Drawing.Point(8, 3);
            this.DgPropaganda.Name = "DgPropaganda";
            this.DgPropaganda.ReadOnly = true;
            this.DgPropaganda.Size = new System.Drawing.Size(772, 375);
            this.DgPropaganda.TabIndex = 0;
            this.DgPropaganda.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgPropaganda_CellDoubleClick);
            this.DgPropaganda.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.DgPropaganda_RowStateChanged);
            this.DgPropaganda.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.DgPropaganda_UserDeletingRow);
            // 
            // Propaganda
            // 
            this.Propaganda.HeaderText = "Propaganda";
            this.Propaganda.Name = "Propaganda";
            this.Propaganda.ReadOnly = true;
            this.Propaganda.Width = 90;
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            this.Id.Width = 41;
            // 
            // Vezes_Ar
            // 
            this.Vezes_Ar.HeaderText = "Vezes ao Ar";
            this.Vezes_Ar.Name = "Vezes_Ar";
            this.Vezes_Ar.ReadOnly = true;
            this.Vezes_Ar.Width = 89;
            // 
            // Cliente
            // 
            this.Cliente.HeaderText = "Cliente";
            this.Cliente.Name = "Cliente";
            this.Cliente.ReadOnly = true;
            this.Cliente.Width = 64;
            // 
            // Data_ini
            // 
            this.Data_ini.HeaderText = "Data Inicio";
            this.Data_ini.Name = "Data_ini";
            this.Data_ini.ReadOnly = true;
            this.Data_ini.Width = 83;
            // 
            // Data_fim
            // 
            this.Data_fim.HeaderText = "Data Fim";
            this.Data_fim.Name = "Data_fim";
            this.Data_fim.ReadOnly = true;
            this.Data_fim.Width = 74;
            // 
            // tempo_init
            // 
            this.tempo_init.HeaderText = "Hora Inicial";
            this.tempo_init.Name = "tempo_init";
            this.tempo_init.ReadOnly = true;
            this.tempo_init.Width = 85;
            // 
            // Tempo_fim
            // 
            this.Tempo_fim.HeaderText = "Hora Final";
            this.Tempo_fim.Name = "Tempo_fim";
            this.Tempo_fim.ReadOnly = true;
            this.Tempo_fim.Width = 80;
            // 
            // Locutor
            // 
            this.Locutor.HeaderText = "Locutor";
            this.Locutor.Name = "Locutor";
            this.Locutor.ReadOnly = true;
            this.Locutor.Width = 68;
            // 
            // Secundagem
            // 
            this.Secundagem.HeaderText = "Secundagem";
            this.Secundagem.Name = "Secundagem";
            this.Secundagem.ReadOnly = true;
            this.Secundagem.Width = 95;
            // 
            // BtAtualizar
            // 
            this.BtAtualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtAtualizar.Location = new System.Drawing.Point(813, 221);
            this.BtAtualizar.Name = "BtAtualizar";
            this.BtAtualizar.Size = new System.Drawing.Size(75, 23);
            this.BtAtualizar.TabIndex = 16;
            this.BtAtualizar.Text = "&Atualizar";
            this.BtAtualizar.UseVisualStyleBackColor = true;
            this.BtAtualizar.Click += new System.EventHandler(this.BtAtualizar_Click);
            // 
            // BtDeletar
            // 
            this.BtDeletar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtDeletar.Location = new System.Drawing.Point(813, 176);
            this.BtDeletar.Name = "BtDeletar";
            this.BtDeletar.Size = new System.Drawing.Size(75, 23);
            this.BtDeletar.TabIndex = 15;
            this.BtDeletar.Text = "&Deletar";
            this.BtDeletar.UseVisualStyleBackColor = true;
            this.BtDeletar.Click += new System.EventHandler(this.BtDeletar_Click);
            // 
            // BtVisualizar
            // 
            this.BtVisualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtVisualizar.Location = new System.Drawing.Point(813, 134);
            this.BtVisualizar.Name = "BtVisualizar";
            this.BtVisualizar.Size = new System.Drawing.Size(75, 23);
            this.BtVisualizar.TabIndex = 14;
            this.BtVisualizar.Text = "&Visualizar";
            this.BtVisualizar.UseVisualStyleBackColor = true;
            this.BtVisualizar.Click += new System.EventHandler(this.BtVisualizar_Click);
            // 
            // BtEditar
            // 
            this.BtEditar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtEditar.Location = new System.Drawing.Point(813, 91);
            this.BtEditar.Name = "BtEditar";
            this.BtEditar.Size = new System.Drawing.Size(75, 23);
            this.BtEditar.TabIndex = 11;
            this.BtEditar.Text = "&Editar";
            this.BtEditar.UseVisualStyleBackColor = true;
            this.BtEditar.Click += new System.EventHandler(this.BtEditar_Click);
            // 
            // BtNovo
            // 
            this.BtNovo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtNovo.Location = new System.Drawing.Point(813, 47);
            this.BtNovo.Name = "BtNovo";
            this.BtNovo.Size = new System.Drawing.Size(75, 23);
            this.BtNovo.TabIndex = 10;
            this.BtNovo.Text = "&Novo";
            this.BtNovo.UseVisualStyleBackColor = true;
            this.BtNovo.Click += new System.EventHandler(this.BtNovo_Click);
            // 
            // Buscar
            // 
            this.Buscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Buscar.Location = new System.Drawing.Point(689, 18);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(91, 23);
            this.Buscar.TabIndex = 9;
            this.Buscar.Text = "&Buscar";
            this.Buscar.UseVisualStyleBackColor = true;
            this.Buscar.Visible = false;
            this.Buscar.Click += new System.EventHandler(this.Buscar_Click);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(530, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Status";
            this.label3.Visible = false;
            // 
            // BuscaStatus
            // 
            this.BuscaStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuscaStatus.FormattingEnabled = true;
            this.BuscaStatus.Items.AddRange(new object[] {
            "Concluido",
            "Finalizados",
            "Em espera"});
            this.BuscaStatus.Location = new System.Drawing.Point(527, 20);
            this.BuscaStatus.Name = "BuscaStatus";
            this.BuscaStatus.Size = new System.Drawing.Size(156, 21);
            this.BuscaStatus.TabIndex = 7;
            this.BuscaStatus.Visible = false;
            // 
            // BuscarCliente
            // 
            this.BuscarCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BuscarCliente.Location = new System.Drawing.Point(357, 21);
            this.BuscarCliente.Name = "BuscarCliente";
            this.BuscarCliente.Size = new System.Drawing.Size(164, 20);
            this.BuscarCliente.TabIndex = 4;
            this.BuscarCliente.Visible = false;
            // 
            // BuscaPropanda
            // 
            this.BuscaPropanda.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BuscaPropanda.Location = new System.Drawing.Point(9, 21);
            this.BuscaPropanda.Name = "BuscaPropanda";
            this.BuscaPropanda.Size = new System.Drawing.Size(342, 20);
            this.BuscaPropanda.TabIndex = 2;
            this.BuscaPropanda.Visible = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(360, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cliente";
            this.label2.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Propaganda";
            this.label1.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.TpPropagandas);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(926, 426);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tBconnection);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.BtMudarConn);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.NuTentativas);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(918, 400);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Configuração";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tBconnection
            // 
            this.tBconnection.Location = new System.Drawing.Point(113, 47);
            this.tBconnection.Name = "tBconnection";
            this.tBconnection.Size = new System.Drawing.Size(708, 20);
            this.tBconnection.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 47);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "ConnectionString";
            // 
            // BtMudarConn
            // 
            this.BtMudarConn.Location = new System.Drawing.Point(113, 73);
            this.BtMudarConn.Name = "BtMudarConn";
            this.BtMudarConn.Size = new System.Drawing.Size(85, 23);
            this.BtMudarConn.TabIndex = 8;
            this.BtMudarConn.Text = "Mudar";
            this.BtMudarConn.UseVisualStyleBackColor = true;
            this.BtMudarConn.Click += new System.EventHandler(this.button3_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 19);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(116, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Tempo entre tentativas";
            // 
            // NuTentativas
            // 
            this.NuTentativas.Enabled = false;
            this.NuTentativas.Location = new System.Drawing.Point(141, 16);
            this.NuTentativas.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NuTentativas.Name = "NuTentativas";
            this.NuTentativas.ReadOnly = true;
            this.NuTentativas.Size = new System.Drawing.Size(120, 20);
            this.NuTentativas.TabIndex = 0;
            this.NuTentativas.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.TsStatusBanco});
            this.statusStrip1.Location = new System.Drawing.Point(0, 404);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(926, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(43, 17);
            this.toolStripStatusLabel1.Text = "Banco:";
            // 
            // TsStatusBanco
            // 
            this.TsStatusBanco.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.TsStatusBanco.LinkColor = System.Drawing.Color.Red;
            this.TsStatusBanco.Name = "TsStatusBanco";
            this.TsStatusBanco.Size = new System.Drawing.Size(42, 17);
            this.TsStatusBanco.Text = "Online";
            // 
            // timerPopular
            // 
            this.timerPopular.Interval = 30000;
            this.timerPopular.Tick += new System.EventHandler(this.timerPopular_Tick);
            // 
            // Adm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 426);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.MinimumSize = new System.Drawing.Size(853, 465);
            this.Name = "Adm";
            this.Text = "Administrador";
            this.Load += new System.EventHandler(this.Adm_Load);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgUser)).EndInit();
            this.TpPropagandas.ResumeLayout(false);
            this.TpPropagandas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgPropaganda)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NuTentativas)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button BtEditarUser;
        private System.Windows.Forms.Button BtNovoUser;
        private System.Windows.Forms.Button BuscarUsuario;
        private System.Windows.Forms.TextBox BuscarNome;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView DgUser;
        private System.Windows.Forms.TabPage TpPropagandas;
        private System.Windows.Forms.Button BtEditar;
        private System.Windows.Forms.Button BtNovo;
        private System.Windows.Forms.Button Buscar;
        private System.Windows.Forms.TextBox BuscarCliente;
        private System.Windows.Forms.TextBox BuscaPropanda;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DgPropaganda;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ComboBox BuscarCargo;
        private System.Windows.Forms.Button BtVisualizarUser;
        private System.Windows.Forms.Button BtDeletar;
        private System.Windows.Forms.Button BtVisualizar;
        private System.Windows.Forms.Button BtDeletarUser;
        private System.Windows.Forms.TextBox BuscarEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel TsStatusBanco;
        private System.Windows.Forms.Button BtNovaSenha;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown NuTentativas;
        private System.Windows.Forms.Button BtMudarConn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tBconnection;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cargo;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox BuscaStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Propaganda;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vezes_Ar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data_ini;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data_fim;
        private System.Windows.Forms.DataGridViewTextBoxColumn tempo_init;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tempo_fim;
        private System.Windows.Forms.DataGridViewTextBoxColumn Locutor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Secundagem;
        private System.Windows.Forms.Button BtAtualizarUser;
        private System.Windows.Forms.Button BtAtualizar;
        private System.Windows.Forms.Timer timerPopular;
    }
}