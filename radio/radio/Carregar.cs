﻿using radio.banco;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace radio
{
    public partial class Carregar : Form
    {
        Carregar c ;
        public Carregar()
        {
            InitializeComponent();
            c = this;
        }

        private void Carregar_Activated(object sender, EventArgs e)
        {
            this.Hide();
            
            if (!Program.connectionStringsSection.SectionInformation.IsProtected)
            {
                //Program.connectionStringsSection.SectionInformation.ProtectSection("connectionStrings");
                //MessageBox.Show("Seu Banco foi comprometido!", "Falha", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Application.Exit(); 
                Program.connectionStringsSection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");

                // Save the encrypted section.
                Program.connectionStringsSection.SectionInformation.ForceSave = true;

                Program.configuration.Save(ConfigurationSaveMode.Full);

                Gerenciador.iniciar();

                Gerenciador.local.Command("CREATE TABLE TipoUsu (Id int NOT NULL,TipoUsu text DEFAULT NULL)").ExecuteNonQuery();

                Gerenciador.local.Command("CREATE TABLE usuarios (Id int NOT NULL, Login text, Email text , Senha text,Tipo int)").ExecuteNonQuery();

                Gerenciador.local.Command("INSERT INTO usuarios (Id, Login, Email, Senha, Tipo) VALUES(1, 'Master', '', 'Master@123', 1)").ExecuteNonQuery();

                Gerenciador.local.Command("CREATE TABLE propagandas (Id int NOT NULL,Propaganda text NOT NULL,Cliente text NOT NULL,  Radio int NOT NULL,  Repetir int NOT NULL,  Data_ini date NOT NULL,  Data_fim date NOT NULL,  Vezes_Ar int NOT NULL,  Secundagem int NOT NULL,  Tempo_ini time NOT NULL,  Tempo_fim time NOT NULL,  nome text NOT NULL,  Locutor int NOT NULL,  Aviso int NOT NULL,  Observacoes text)").ExecuteNonQuery();

            }
            else
            {
                Gerenciador.iniciar();
            }

            
            login login = new login();
            login.ShowDialog();
            this.Close();

        }

        private void Carregar_Load(object sender, EventArgs e)
        {

        }
    }
}
