﻿using MySql.Data.MySqlClient;
using radio.banco;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace radio
{
    public partial class Alocutar : Form
    {
        public Alocutar()
        {
            InitializeComponent();
        }

        private void Alocutar_Load(object sender, EventArgs e)
        {            

            TsStatusBanco.Text = Gerenciador.StatusOnline;
            Gerenciador.StatusChanged += new EventHandler<StatusEventArgs>(OnStatusEvent);
            popularProp();
        }
        private void OnStatusEvent(object sender, StatusEventArgs e)
        {
            TsStatusBanco.Text = e.Status;
        }

        private void popularProp()
        {

            if (Gerenciador.online.conexao.State == ConnectionState.Open)
            {
                using (IDbCommand dbCommand = Gerenciador.online.Command("SELECT * FROM propagandas WHERE Data_ini <= '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND Data_fim >= '" + DateTime.Now.ToString("yyyy-MM-dd") + "' AND Tempo_ini <= '" + DateTime.Now.ToString("HH:mm") + "' AND Tempo_fim >= '" + DateTime.Now.ToString("HH:mm") + "' ORDER BY Tempo_fim DESC"))
                {
                    try
                    {
                        var result = dbCommand.ExecuteReader();
                        DgPropaganda.Rows.Clear();
                        int i = 0;
                        while (result.Read())
                        {
                            
                                DgPropaganda.Rows.Add();

                                DgPropaganda.Rows[i].Cells["Id"].Value = Convert.ToInt32(result["Id"]);

                                DgPropaganda.Rows[i].Cells["Propaganda"].Value = result["nome"];

                                DgPropaganda.Rows[i].Cells["Detalhes"].Value = result["Propaganda"];

                                DgPropaganda.Rows[i].Cells["Cliente"].Value = result["Cliente"];

                                DgPropaganda.Rows[i].Cells["Data_ini"].Value = Convert.ToDateTime(result["Data_ini"]);

                                DgPropaganda.Rows[i].Cells["Data_fim"].Value = Convert.ToDateTime(result["Data_fim"]);

                                DgPropaganda.Rows[i].Cells["tempo_init"].Value = result["Tempo_ini"];

                                DgPropaganda.Rows[i].Cells["Tempo_fim"].Value = result["Tempo_fim"];

                                if ((int)result["Locutor"] == 1)
                                {
                                    DgPropaganda.Rows[i].Cells["Locutor"].Value = "Felinino";
                                }
                                else if ((int)result["Locutor"] == 2)
                                {
                                    DgPropaganda.Rows[i].Cells["Locutor"].Value = "Masculino";
                                }
                                else if ((int)result["Locutor"] == 3)
                                {
                                    DgPropaganda.Rows[i].Cells["Locutor"].Value = "Ambos";
                                }
                                else
                                {
                                    DgPropaganda.Rows[i].Cells["Locutor"].Value = "Outro";
                                }

                                DgPropaganda.Rows[i].Cells["Secundagem"].Value = result["Secundagem"];

                                DgPropaganda.Rows[i].Cells["Vezes_Ar"].Value = result["Vezes_Ar"];

                                i += 1;
                            
                        }
                        result.Close();
                        Gerenciador.Sincronizar();

                    }
                    catch (MySqlException)
                    {
                        if (Gerenciador.online.conexao.State != ConnectionState.Open)
                        {
                            popularProp();
                        }
                    }
                }
            }
            else using (IDbCommand dbCommand = Gerenciador.local.Command("SELECT * FROM propagandas WHERE Data_ini <= '"+ DateTime.Now.ToString("yyyy-MM-dd") + "' AND Data_fim >= '"+DateTime.Now.ToString("yyyy-MM-dd") + "' AND Tempo_ini <= '" + DateTime.Now.ToString("HH:mm") + "' AND Tempo_fim >= '" + DateTime.Now.ToString("HH:mm") + "' ORDER BY Tempo_fim DESC"))
                {
                    dbCommand.Dispose();
                    var result = dbCommand.ExecuteReader();
                    DgPropaganda.Rows.Clear();
                    int i = 0;
                    while (result.Read())
                    {
                        
                            DgPropaganda.Rows.Add();

                            DgPropaganda.Rows[i].Cells["Id"].Value = Convert.ToInt32(result["Id"]);

                            DgPropaganda.Rows[i].Cells["Propaganda"].Value = result["nome"];

                            DgPropaganda.Rows[i].Cells["Cliente"].Value = result["Cliente"];

                            DgPropaganda.Rows[i].Cells["Data_ini"].Value = Convert.ToDateTime(result["Data_ini"]);

                            DgPropaganda.Rows[i].Cells["Data_fim"].Value = Convert.ToDateTime(result["Data_fim"]);

                            DgPropaganda.Rows[i].Cells["tempo_init"].Value = result["Tempo_ini"];

                            DgPropaganda.Rows[i].Cells["Tempo_fim"].Value = result["Tempo_fim"];

                            if ((int)result["Locutor"] == 1)
                            {
                                DgPropaganda.Rows[i].Cells["Locutor"].Value = "Felinino";
                            }
                            else if ((int)result["Locutor"] == 2)
                            {
                                DgPropaganda.Rows[i].Cells["Locutor"].Value = "Masculino";
                            }
                            else if ((int)result["Locutor"] == 3)
                            {
                                DgPropaganda.Rows[i].Cells["Locutor"].Value = "Ambos";
                            }
                            else
                            {
                                DgPropaganda.Rows[i].Cells["Locutor"].Value = "Outro";
                            }

                            DgPropaganda.Rows[i].Cells["Secundagem"].Value = result["Secundagem"];

                            DgPropaganda.Rows[i].Cells["Vezes_Ar"].Value = result["Vezes_Ar"];

                            i += 1;
                        
                    }
                    result.Close();
                }

        }


        private void BtAtualizar_Click(object sender, EventArgs e)
        {
            popularProp();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Locultar loc = new Locultar(DgPropaganda.Rows);
            loc.ShowDialog();
        }
    }
}
