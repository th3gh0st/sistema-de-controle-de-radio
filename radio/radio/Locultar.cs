﻿using radio.banco;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace radio
{
    public partial class Locultar : Form
    {
        DataGridViewRowCollection dt;
        public Locultar(DataGridViewRowCollection mDt)
        {
            dt = mDt;
            InitializeComponent();
        }
        int pos = 0;
        List<int> falta = new List<int>();

        private void Locultar_Load(object sender, EventArgs e)
        {

            TsStatusBanco.Text = Gerenciador.StatusOnline;
            Gerenciador.StatusChanged += new EventHandler<StatusEventArgs>(OnStatusEvent);
            Proximo();
        }

        private void Proximo(Boolean pular = false)
        {
            if(falta.Count != 0 && (!pular || pos == dt.Count))
            {
                Propaganda.Text = (string)dt[falta[0]].Cells["Detalhes"].Value;
                locutor.Text = (string)dt[falta[0]].Cells["Locutor"].Value;
                locutor.Text = (string)dt[falta[0]].Cells["Locutor"].Value;
                contAtual.Text = (falta[0] + 1).ToString();
                Cont.Text = dt.Count.ToString();
                falta.Remove(falta[0]);
            }
            else
            {
                if(pos <= dt.Count-1)
                {
                    Propaganda.Text = (string)dt[pos].Cells["Detalhes"].Value;
                    locutor.Text = (string)dt[pos].Cells["Locutor"].Value;
                    locutor.Text = (string)dt[pos].Cells["Locutor"].Value;
                    contAtual.Text = (pos + 1).ToString();
                    Cont.Text = dt.Count.ToString();
                    pos++;
                }
                else
                {
                    MessageBox.Show("A lista de propagandas foi concluida!");
                    this.Close();
                }                  
            }

        }
           
        private void OnStatusEvent(object sender, StatusEventArgs e)
        {
            TsStatusBanco.Text = e.Status;
        }

        private void BtAvancar_Click(object sender, EventArgs e)
        {
            Proximo();
        }

        private void BtPular_Click(object sender, EventArgs e)
        {
            falta.Add(pos-1);
            Proximo(true);
        }
    }
}
