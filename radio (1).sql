-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 30-Jun-2018 às 23:59
-- Versão do servidor: 10.1.31-MariaDB
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `radio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `propagandas`
--

CREATE TABLE `propagandas` (
  `Id` int(11) NOT NULL,
  `Propaganda` text NOT NULL,
  `Cliente` text NOT NULL,
  `Radio` int(11) NOT NULL,
  `Repetir` int(11) NOT NULL,
  `Data_ini` date NOT NULL,
  `Data_fim` date NOT NULL,
  `Vezes_Ar` int(11) NOT NULL,
  `Secundagem` int(11) NOT NULL,
  `Tempo_ini` time NOT NULL,
  `Tempo_fim` time NOT NULL,
  `nome` text NOT NULL,
  `Locutor` int(11) NOT NULL,
  `Aviso` int(11) NOT NULL,
  `Observacoes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `propagandas`
--

INSERT INTO `propagandas` (`Id`, `Propaganda`, `Cliente`, `Radio`, `Repetir`, `Data_ini`, `Data_fim`, `Vezes_Ar`, `Secundagem`, `Tempo_ini`, `Tempo_fim`, `nome`, `Locutor`, `Aviso`, `Observacoes`) VALUES
(20, 'qeewe', 'ee', 1, 0, '2018-06-22', '2018-06-30', 0, 15, '00:00:00', '23:00:00', 'ee', 2, 0, 'wqee');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipousu`
--

CREATE TABLE `tipousu` (
  `Id` int(11) NOT NULL,
  `TipoUsu` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipousu`
--

INSERT INTO `tipousu` (`Id`, `TipoUsu`) VALUES
(1, 'Administrador'),
(2, 'Locutor');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `Id` int(11) NOT NULL,
  `Login` text,
  `Email` text NOT NULL,
  `Senha` text,
  `Tipo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`Id`, `Login`, `Email`, `Senha`, `Tipo`) VALUES
(1, 'Master', '', 'Master@123', 1),
(5, 'Locutor', 'locutor@locutor.com', 'Locutor@123', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `propagandas`
--
ALTER TABLE `propagandas`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tipousu`
--
ALTER TABLE `tipousu`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_Usuarios_1` (`Tipo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `propagandas`
--
ALTER TABLE `propagandas`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_Usuarios_1` FOREIGN KEY (`Tipo`) REFERENCES `tipousu` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
