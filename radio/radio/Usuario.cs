﻿using MySql.Data.MySqlClient;
using radio.banco;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace radio
{
    public partial class Usuario : Form
    {
        int type;
        int id;
        public Usuario(int mType = 0, int mId = 0)
        {
            type = mType;
            id = mId;
            InitializeComponent();
        }

        private void Usuario_Load(object sender, EventArgs e)
        {
            if(type != 0)
            {
                popularUser();
            }

            if (type == 2)
            {
                alternarBloqueio();
                BtSalvarEditar.Text = "&Editar";
                
            }
            TsStatusBanco.Text = Gerenciador.StatusOnline;
            Gerenciador.StatusChanged += new EventHandler<StatusEventArgs>(OnStatusEvent);    
        }

        private void popularUser()
        {

            if (Gerenciador.online.conexao.State == ConnectionState.Open)
            {
                using (IDbCommand dbCommand = Gerenciador.online.Command("SELECT * FROM usuarios WHERE Id = "+id))
                {
                    try
                    {
                        var result = dbCommand.ExecuteReader();
                        int i = 0;
                        while (result.Read())
                        {

                            

                            TbNome.Text = Convert.ToString(result["Login"]);

                            TbEmail.Text = Convert.ToString(result["Email"]);

                            CbCargo.SelectedIndex = Convert.ToInt16(result["Tipo"])-1;
                            i += 1;
                        }
                    }
                    catch (MySqlException)
                    {
                        if (Gerenciador.online.conexao.State != ConnectionState.Open)
                        {
                            popularUser();
                        }
                    }
                }
            }
            else using (IDbCommand dbCommand = Gerenciador.local.Command("SELECT * FROM usuarios WHERE Id = " + id))
                {
                    var result = dbCommand.ExecuteReader();
                    int i = 0;
                    while (result.Read())
                    {

                        TbNome.Text = Convert.ToString(result["Login"]);

                        TbEmail.Text = Convert.ToString(result["Email"]);

                        CbCargo.SelectedIndex = Convert.ToInt16(result["Tipo"]) - 1;
                        i += 1;
                    }
                    result.Close();
                }


        }


        private void OnStatusEvent(object sender, StatusEventArgs e)
        {
            if (BtSalvarEditar.InvokeRequired)
                BtSalvarEditar.BeginInvoke((MethodInvoker)delegate {
                    TsStatusBanco.Text = e.Status;
                    if (e.Status != "Conectado")
                    {
                        BtSalvarEditar.Enabled = false;
                    }
                    else
                    {
                        BtSalvarEditar.Enabled = true;
                    }
                });
        }


        private void alternarBloqueio()
        {
            TbNome.ReadOnly = !TbNome.ReadOnly;
            TbEmail.ReadOnly = !TbEmail.ReadOnly;
            CbCargo.Enabled = !CbCargo.Enabled;
        }

        private void BtSalvarEditar_Click(object sender, EventArgs e)
        {
            if (type == 2 && TbNome.ReadOnly)
            {
                alternarBloqueio();
                BtSalvarEditar.Text = "&Salvar";
            }
            else
            {

                if (TbNome.Text == "" ||
                   TbEmail.Text == "" ||                  
                   (string)CbCargo.SelectedItem is null )
                {
                    MessageBox.Show("Todos os campos devem ser preenchidos");

                }
                else
                {
                    String sql = "";
                    if (type == 0)
                    {
                        sql = "INSERT INTO usuarios ( Login, Email, Senha, Tipo) VALUES " +
                           "( @Login, @Email, @Senha, @Tipo);";

                    }
                    else
                    {
                        sql = "UPDATE usuarios SET Login = @Login, Email = @Email, Tipo = @Tipo WHERE usuarios.Id = "+id;
                    }
                    var command = Gerenciador.online.Command(sql);
                    Dictionary<string, object> values = new Dictionary<string, object>();

                    values["@Login"] = TbNome.Text;
                    values["@Email"] = TbEmail.Text;
                    values["@Senha"] = "Mudar@123";
                    values["@Tipo"] = CbCargo.SelectedIndex+1;                  

                    foreach (var item in values)
                    {
                        var p = command.CreateParameter();
                        p.ParameterName = item.Key;
                        p.Value = item.Value;
                        command.Parameters.Add(p);
                    }
                    
                    try
                    {                        
                        command.ExecuteNonQuery();
                        Gerenciador.Sincronizar();
                        MessageBox.Show("Salvo com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (type == 0)
                        {
                            MessageBox.Show("A Senha temporaria é 'Mudar@123' ", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        this.Close();
                    }
                    catch (MySqlException)
                    {

                        MessageBox.Show("Não Foi possivel salvar!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    

                }
            }
        }
    }
}
